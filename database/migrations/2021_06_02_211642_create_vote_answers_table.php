<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoteAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vote_answers', function (Blueprint $table) {
            $table->id();
            $table->double('answer1', 100, 1)->nullable();
            $table->double('answer2', 100, 1)->nullable();
            $table->double('answer3', 100, 1)->nullable();
            $table->double('answer4', 100, 1)->nullable();
            $table->double('answer5', 100, 1)->nullable();
            $table->double('answer6', 100, 1)->nullable();
            $table->double('answer7', 100, 1)->nullable();
            $table->double('answer8', 100, 1)->nullable();
            $table->double('answer9', 100, 1)->nullable();
            $table->double('answer10', 100, 1)->nullable();
            $table->double('sum', 100, 1)->nullable();
            $table->unsignedBigInteger('vote_id');
            $table->foreign('vote_id')->references('id')->on('votes')->onDelete('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('advisor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vote_answers');
    }
}
