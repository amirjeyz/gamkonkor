<?php

use Illuminate\Support\Facades\Route;

if (!function_exists('isActive')) {
    function isActive($key, $activeClassName = 'active')
    {
        if (is_array($key)) {
            return in_array(Route::currentRouteName(), $key) ? $activeClassName : '';
        }
        return Route::currentRouteName() == $key ? $activeClassName : '';
    }
}


if (!function_exists('isOpen')) {
    function isOpen($key, $activeClassName = 'menu-open')
    {
        if (is_array($key)) {
            return in_array(Route::currentRouteName(), $key) ? $activeClassName : '';
        }
        return Route::currentRouteName() == $key ? $activeClassName : '';
    }
}

if (!function_exists('roleToPersian')) {
    function roleToPersian($role)
    {
        $roles = [
            'Student' => 'دانش آموز',
            'Admin' => 'ادمین',
            'Teacher' => 'مدرس',
            'Advisor' => 'مشاور'
        ];
        return $roles[$role];
    }
}
