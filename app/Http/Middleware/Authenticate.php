<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('auth.login');
        } else {
            if (Auth::user()->hasRole('Admin')) {
                return redirect('/admin');
            } elseif (Auth::user()->hasRole('Advisor')) {
                return redirect('/advisor');
            } elseif (Auth::user()->hasRole('Student')) {
                return redirect('/student');
            } else {
                abort(404);
            }
        }
    }
}
