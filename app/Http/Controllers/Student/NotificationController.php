<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function redirect($id)
    {
        $user = User::find(Auth::user()->id);

        foreach ($user->unreadNotifications as $notify) {
            $url = $notify->data['url'];
        }

        $user->unreadNotifications->where('id', $id)->markAsRead();

        return redirect($url);
    }
}
