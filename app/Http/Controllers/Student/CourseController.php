<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index()
    {
        $checkOrders = Order::where('user_id', Auth::user()->id)
                            ->where([
                                'status' => 'paid',
                                'type' => 'course'
                                ])
                            ->get();
        return view('student.courses.index', compact('checkOrders'));
    }
}
