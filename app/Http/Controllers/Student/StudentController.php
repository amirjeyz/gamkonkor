<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;

class StudentController extends Controller
{
    public $user;
    public function __construct()
    {
        $this->user = auth()->user();
    }


    public function profile()
    {
        return view('student.profile.profile', [
            'user' => auth()->user()
        ]);
    }

    public function edit()
    {
        return view('student.profile.edit', [
            'user' => auth()->user()
        ]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'string|min:5|max:150',
            'phone' => 'min:5|' . Rule::unique('users', 'phone')->ignore($user->id),
            'field' => 'string',
            'city' => 'string',
            'parent_phone' => 'min:5',
            'email' => 'nullable|email|max:250',
            'grade' => 'string',
            'file' => 'mimes:jpeg,jpg,png,gif|max:2048'
        ]);
        $data = [
            'grade' => $request->input('grade'),
            'field' => $request->input('field'),
            'email' => $request->input('email'),
            'city' => $request->input('city'),
            'parent_phone' => $request->input('parent_phone'),
            'user_id' => $user->id
        ];

        if ($request->has('file')) {
            $file = $request->file('file');
            $destinationPath = 'storage/avatar/' . now()->year . '/' . now()->month . '/';
            $filename =  $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $data['file'] = $destinationPath . $filename;
        }

        $user->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
        ]);

        if ($user->student == null) {
            Student::create($data);
        } else {
            $user->student->update($data);
        }

        toast('اطلاعات حساب کاربری شما با موفقیت بروزرسانی شد.', 'success');
        return redirect(route('student.profile'));
    }

    public function newPassword()
    {
        return view('student.profile.new-password', [
            'user' => auth()->user()
        ]);
    }

    public function updatePassword(Request $request, User $user)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
        $user->update([
            'password' => Hash::make($request->input('password'))
        ]);
        toast('رمز عبور شما بروزرسانی شد.', 'info');
        return redirect(route('student.profile'));
    }

}
