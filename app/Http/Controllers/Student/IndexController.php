<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\AssignPackage;
use App\Models\Package;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        if ($user->student != null && $user->student->advisor_id != null) {
            $advisor_id = $user->student->advisor_id;
            $advisor = \App\Models\User::where('id', $advisor_id)->with('advisor')->first();
        } else {
            $advisor = null;
        }

        $packagesID = [];
        foreach ($user->packages as $package) {
            $packagesID[] = $package->id;
        }

        $packages = Package::whereIn('id', $packagesID)->get();
        $packagesStatus = [];
        foreach ($packages as $package) {
            foreach ($package->users as $userHasPackage) {
                if ($userHasPackage->pivot->user_id == Auth::user()->id) {
                    $now = Carbon::now();
                    $expired = $userHasPackage->pivot->expired_at;
                    $diff = $now->diffInDays($expired);
                    $packagesStatus[] = [
                        'packageTitle' => $package->title,
                        'days' => $diff
                    ];
                }
            }
        }

        return view('student.index', compact([
            'advisor',
            'packagesStatus'
        ]));
    }
}
