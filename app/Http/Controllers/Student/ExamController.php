<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ExamController extends Controller
{
    public function index()
    {
        return view('student.exam.index', [
            'exams' => Auth::user()->studentsQuizzes
        ]);
    }

    public function showSingle($id)
    {
        $quiz = Quiz::where('id', $id)
                    ->where('student_id', Auth::user()->id)
                    ->where('status', 1)
                    ->first();

        if (
            Quiz::where('id', $id)
            ->where('student_id', Auth::user()->id)
            ->where('status', 1)
            ->first()
            ) {
            return view('student.exam.single');
        } elseif (
            Quiz::where('id', $id)
            ->where('student_id', Auth::user()->id)
            ->where('status', 2)
            ->first()
            ) {
                Alert::error('متاسفیم‌!', 'آزمون به پایان رسیده است.')->autoClose(5000)->showConfirmButton('خیلی خب');
                return redirect(route('student.exam.index'));
        } else {
            abort(404);
        }

    }

}
