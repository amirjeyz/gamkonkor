<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Vote;
use App\Models\VoteAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class VoteController extends Controller
{
    public function vote($id)
    {
        $vote = Vote::find($id);
        return view('student.vote.answer', compact('vote'));
    }

    public function submit(Request $request, $id)
    {
        $has = VoteAnswer::where('vote_id', $id)->where('user_id', '3')->first();
        if ($has) {
            return 'shoma in vote ro javab dadin'; /// badan allow ro bezar jaye in!!!
        }

        $vote = new VoteAnswer();
        $vote->answer1 = $request->answer1 ?? null;
        $vote->answer2 = $request->answer2 ?? null;
        $vote->answer3 = $request->answer3 ?? null;
        $vote->answer4 = $request->answer4 ?? null;
        $vote->answer5 = $request->answer5 ?? null;
        $vote->answer6 = $request->answer6 ?? null;
        $vote->answer7 = $request->answer7 ?? null;
        $vote->answer8 = $request->answer8 ?? null;
        $vote->answer9 = $request->answer9 ?? null;
        $vote->answer10 = $request->answer10 ?? null;
        $sum = $request->answer1 + $request->answer2 + $request->answer3 + $request->answer4 + $request->answer5 + $request->answer6 + $request->answer7 + $request->answer8 + $request->answer9 + $request->answer10;
        $sum = $sum / 10;
        $vote->sum = $sum;
        $vote->vote_id = $id;
        $vote->user_id = Auth::user()->id;
        $vote->advisor_id = Auth::user()->student->advisor_id;
        $vote->save();

        Alert::success('نظرسنجی با موفقیت ثبت شد!', '')->autoClose(5000)->showConfirmButton('خیلی خب!');
        return redirect(route('student.index'));
    }






}
