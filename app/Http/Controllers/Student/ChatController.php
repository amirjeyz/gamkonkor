<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ChatController extends Controller
{
    public function list()
    {
        $tickets = Ticket::where('user_id', Auth::user()->id)
                        ->groupBy('ticket_id')
                        ->get();

        return view('student.chat.list', [
            'tickets' => $tickets
        ]);
    }

    public function index($ticket_id)
    {

        if (! Ticket::where('ticket_id', $ticket_id)->first()) {
            abort(404);
        }
        $tickets = Ticket::where('ticket_id', $ticket_id)->get();
        $ticket = Ticket::where('ticket_id', $ticket_id)->with('receiver')->first();

        return view('student.chat.index', compact('tickets', 'ticket_id', 'ticket'));
    }

    public function sendMessage(Request $request)
    {
        $lastData = Ticket::where('ticket_id', $request->ticket_id)->first();
        $send = Ticket::create([
            'user_id' => Auth::user()->id,
            'category_id' => $lastData->category_id,
            'receiver_id' => $lastData->receiver_id,
            'title' => $lastData->title,
            'body' => $request->body,
            'status' => '1',
            'ticket_id' => $request->ticket_id
        ]);

        return back();
    }
}
