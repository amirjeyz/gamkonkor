<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\AssignPackage;
use App\Models\Order;
use App\Models\Package;
use App\Models\Payment;
use App\Models\Student;
use App\Models\Ticket;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as ShetabitPayment;

class PackageController extends Controller
{
    public function index()
    {
        return view('student.packages.buy', [
            'packages' => Package::all()
        ]);
    }

    public function storeBuy(Request $request)
    {

        $user = Auth::user();

        foreach ($user->packages as $package) {
            if ($package->pivot->package_id == $request->package) {
                Alert::warning('', 'شما قادر به خرید این پکیج نمی‌باشید.');
                return back();
            }
        }

        $package = Package::whereId($request->package)->first();

        $advisorID = 0;
        if ($request->advisor) {
            $advisorID = $request->advisor;
        }
        $teacher = 0;
        if ($request->teacher) {
            $teacher = $request->teacher;
        }
        $psychologist = 0;
        if ($request->psychologist) {
            $psychologist = $request->psychologist;
        }

        $order = Order::create([
            'user_id' => Auth::user()->id,
            'price' => $package->price,
            'type' => 'package',
            'status' => 'unpaid'
        ]);

        $order->packages()->attach($package->id);

        $invoice = (new Invoice())->amount($order->price);

        // Purchase and pay the given invoice.
        // You should use return statement to redirect user to the bank page.
        return ShetabitPayment::via('payping')->callbackUrl(route('student.package.callback', [
            'advisorID' => $advisorID,
            'teacher' => $teacher,
            'psychologist' => $psychologist,
            'lesson_id' => $request->lesson
        ]))->purchase($invoice, function($driver, $transactionId) use ($order, $invoice) {
            \App\Models\Payment::create([
                'order_id' => $order->id,
                'resnumber' => $invoice->getUuid()
            ]);
        })->pay()->render();
    }

    public function Callback(Request $request)
    {
        try {
            $payment = Payment::where('resnumber', $request->clientrefid)->firstOrFail();
            // $payment->order->price
            $receipt = ShetabitPayment::amount($payment->order->price)->transactionId($request->clientrefid)->verify();

            $payment->update([
                'status' => 1,
            ]);

            $payment->order()->update([
                'status' => 'paid'
            ]);

            $user = Auth::user();
            $order = $payment->order;

            foreach ($order->packages as $item) {
                $package = $item;
            }

            $permissions = [];
            foreach ($package->permissions as $item) {
                $permissions[] = $item->name;
            }

            if (in_array('psychology-pkg', $permissions)) {
                Student::whereUserId($user->id)->update(['psychologist_id' => $request->psychologist]);
                $psychologistName = User::where('id', $request->psychologist)->first()->name;
                Ticket::create([
                    'user_id' => Auth::user()->id,
                    'category_id' => 1,
                    'receiver_id' => $request->psychologist,
                    'title' => "ارتباط پیامی با {$psychologistName}",
                    'body' => '',
                    'status' => 1,
                    'ticket_id' => 'GK-' . random_int(100000, 999999),
                ]);
            }
            if (in_array('debugging-pkg', $permissions)) {
                Student::whereUserId($user->id)->update(['teacher_id' => $request->teacher]);
                $teachertName = User::where('id', $request->teacher)->first()->name;
                $lessonName = User::where('id', $request->lesson_id)->first()->name_fa;

                Ticket::create([
                    'user_id' => Auth::user()->id,
                    'category_id' => 1,
                    'receiver_id' => $request->teacher,
                    'title' => "ارتباط پیامی با {$teachertName} برای درس {$lessonName}",
                    'body' => '',
                    'status' => 1,
                    'ticket_id' => 'GK-' . random_int(100000, 999999),
                ]);
            }
            if (in_array('academic-advice-pkg', $permissions)) {
                Student::whereUserId($user->id)->update(['advisor_id' => $request->advisorID]);
                $advisorName = User::where('id', $request->advisorID)->first()->name;
                Ticket::create([
                    'user_id' => Auth::user()->id,
                    'category_id' => 1,
                    'receiver_id' => $request->advisorID,
                    'title' => "ارتباط پیامی با {$advisorName}",
                    'body' => '',
                    'status' => 1,
                    'ticket_id' => 'GK-' . rand(100000, 999999),
                ]);
            }

            $assignUserToPackage = $package->users()->attach($user->id, [
                'expired_at' => Carbon::now()->addMonth($package->expire_time)
            ]);

            $user->givePermissionTo($permissions);

            Alert::success('', 'حساب شما با موفقیت تمدید شد.');
            return redirect(route('student.package.store'));

        } catch (InvalidPaymentException $exception) {
            /**
            when payment is not verified, it will throw an exception.
            We can catch the exception to handle invalid payments.
            getMessage method, returns a suitable message that can be used in user interface.
             **/
            Alert::error('', $exception->getMessage());
            return redirect(route('student.package.store'));
        }
    }

    public function CallbackTest(Request $request)
    {
        $payment = Payment::where('resnumber', 'fdaa0661-046b-48b6-85f5-38e0c39455db')->firstOrFail();

        // $payment->order->price
        // $receipt = ShetabitPayment::amount($payment->order->price)->transactionId($request->clientrefid)->verify();
        $payment->update([
            'status' => 1,
        ]);

        $payment->order()->update([
            'status' => 'paid'
        ]);

        $user = Auth::user();
        $order = $payment->order;

        foreach ($order->packages as $item) {
            $package = $item;
        }

        $permissions = [];
        foreach ($package->permissions as $item) {
            $permissions[] = $item->name;
        }

        if (in_array('psychology-pkg', $permissions)) {
            Student::whereUserId($user->id)->update(['psychologist_id' => $request->psychologist]);
        }
        if (in_array('debugging-pkg', $permissions)) {
            Student::whereUserId($user->id)->update(['teacher_id' => $request->teacher]);
        }
        if (in_array('academic-advice-pkg', $permissions)) {
            Student::whereUserId($user->id)->update(['advisor_id' => $request->advisorID]);
        }

        $assignUserToPackage = $package->users()->attach($user->id, [
            'expired_at' => Carbon::now()->addMonth($package->expire_time)
        ]);

        $user->givePermissionTo($permissions);
    }
}
