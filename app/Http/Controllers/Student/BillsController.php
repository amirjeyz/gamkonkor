<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\AssignPackage;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as ShetabitPayment;

class BillsController extends Controller
{
    public function index()
    {
        $orders = Order::where('user_id', Auth::user()->id)->get();

        return view('student.bills.index', compact('orders'));
    }

    public function billPayment($id)
    {
        $order = Order::find($id);


        // Create new invoice.
        $invoice = (new Invoice())->amount($order->price);

        // Purchase and pay the given invoice.
        // You should use return statement to redirect user to the bank page.
        return ShetabitPayment::via('payping')->callbackUrl(route('student.bills.callback'))->purchase($invoice, function($driver, $transactionId) use ($order, $invoice) {
            \App\Models\Payment::create([
                'order_id' => $order->id,
                'resnumber' => $invoice->getUuid()
            ]);
        })->pay()->render();
    }

    public function billCallback(Request $request)
    {
        try {
            $payment = Payment::where('resnumber', $request->clientrefid)->firstOrFail();
            // $payment->order->price
            $receipt = ShetabitPayment::amount($payment->order->price)->transactionId($request->clientrefid)->verify();

            $payment->update([
                'status' => 1,
            ]);

            $payment->order()->update([
                'status' => 'paid'
            ]);

            if ($payment->order->type == 'package') {
                foreach ($payment->order->packages as $package) {
                    switch ($package->id) {
                        case "1":
                            $permission = 'academic-advice-pkg';
                            break;
                        case "2":
                            $permission = 'debugging-pkg';
                            break;
                        case "3":
                            $permission = 'exam';
                            break;
                        case "4":
                            $permission = 'psychology-pkg';
                            break;
                    }
                }

                $user = User::where('id',3)->with('permissions')->first();
                foreach ($user->permissions as $userPermission) {
                    if ($userPermission->name == $permission) {
                        $checkUserHasPackage = AssignPackage::where('permission', $permission)->first();
                        $now = Carbon::now();
                        $daysLeft = $now->diffInDays($checkUserHasPackage->expired_at);

                        $create = AssignPackage::create([
                            'user_id' => '3', //Auth::user()->id
                            'package_id' => $package->id,
                            'payment_time' => $payment->created_at,
                            'expired_at' => Carbon::make($payment->created_at)->addMonth(1)->addDays($daysLeft),
                            'permission' => $permission,
                        ]);

                        $checkUserHasPackage->delete();
                    } else {
                        $create = AssignPackage::create([
                            'user_id' => '3', //Auth::user()->id
                            'package_id' => $package->id,
                            'payment_time' => $payment->created_at,
                            'expired_at' => Carbon::make($payment->created_at)->addMonth(1),
                            'permission' => $permission,
                        ]);
                    }
                }


            }

            Alert::success('', 'حساب شما با موفقیت تمدید شد.');
            return redirect(route('student.bills.index'));
        } catch (InvalidPaymentException $exception) {
            /**
            when payment is not verified, it will throw an exception.
            We can catch the exception to handle invalid payments.
            getMessage method, returns a suitable message that can be used in user interface.
             **/
            Alert::error('', $exception->getMessage());
            return redirect(route('student.bills.index'));
        }
    }
}
