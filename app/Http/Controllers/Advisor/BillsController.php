<?php

namespace App\Http\Controllers\Advisor;

use App\Http\Controllers\Controller;
use App\Models\AdvisorSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillsController extends Controller
{
    public function index()
    {
        $salaries = AdvisorSalary::where('user_id', Auth::user()->id)->get();

        return view('advisor.bills.index', compact('salaries'));
    }
}
