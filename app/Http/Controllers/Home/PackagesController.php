<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    public function single($slug)
    {
        if (! Package::where('slug', $slug)->first()) {
            abort(404);
        }

        $package = Package::where('slug', $slug)->first();
        return $package;
    }
}
