<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Episode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function index()
    {
        return view('home.courses.index', [
            'courses' => Course::all()
        ]);
    }

    public function courseSingle($id)
    {
        if (! Course::find($id)) {
            abort(403);
        }


        if (Auth::check()) {
            foreach (Auth::user()->orders as $order) {
                foreach ($order->courses as $course) {
                    $array[] = $course->id;
                }
            }
        }

        $array = [];

        $course = Course::find($id);
        return view('home.courses.single', compact('course', 'array'));

    }

    public function singleEpisode(Request $request)
    {
        if (!Course::find($request->course) || !Episode::find($request->episode)) {
            abort(404);
        }

        $course = Course::where('id', $request->course)
                        ->with('episodeHa')
                        ->first();
        foreach ($course->episodeHa as $episode) {
            $episode = $episode;
        }

        return view('home.courses.episode', compact('episode', 'course'));
    }
}
