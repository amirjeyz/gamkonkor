<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\AssignPackage;
use App\Models\Course;
use App\Models\Payment;
use Carbon\Carbon;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as ShetabitPayment;


class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        $course = Course::find($request->course_id);

        $order = \App\Models\Order::create([
            'user_id' => Auth::user()->id,
            'price' => $course->price,
            'type' => 'course',
            'status' => 'unpaid',
        ]);

        $order->courses()->attach($course->id);

        // Create new invoice.
        $invoice = (new Invoice())->amount($order->price);

        // Purchase and pay the given invoice.
        // You should use return statement to redirect user to the bank page.
        return ShetabitPayment::via('payping')->callbackUrl(route('payment.callback'))->purchase($invoice, function($driver, $transactionId) use ($order, $invoice) {

            \App\Models\Payment::create([
                'order_id' => $order->id,
                'resnumber' => $invoice->getUuid()
            ]);
        })->pay()->render();
    }



    public function callback(Request $request)
    {
        try {
            $payment = Payment::where('resnumber', $request->clientrefid)->firstOrFail();
            // $payment->order->price
            $receipt = ShetabitPayment::amount($payment->order->price)->transactionId($request->clientrefid)->verify();

            $payment->update([
                'status' => 1,
            ]);

            $payment->order()->update([
                'status' => 'paid'
            ]);

            return 'ok';
//            return redirect(route('users.index'));
        } catch (InvalidPaymentException $exception) {
            /**
            when payment is not verified, it will throw an exception.
            We can catch the exception to handle invalid payments.
            getMessage method, returns a suitable message that can be used in user interface.
             **/
//            alert()->error('', $exception->getMessage());
            return $exception->getMessage();


            return redirect(route('users.index'));
        }
    }

}
