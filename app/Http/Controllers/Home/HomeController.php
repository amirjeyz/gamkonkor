<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $courses = Course::with('orders')
                        ->orderBy('created_at', 'DESC')
                        ->limit(4)
                        ->get();

        return view('home.index', compact('courses'));
    }
}
