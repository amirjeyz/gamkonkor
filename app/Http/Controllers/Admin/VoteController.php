<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Vote;
use App\Notifications\SendVoteNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class VoteController extends Controller
{
    public function index()
    {
        $votes = Vote::with('answers')->get();
        $countAnswer = 0;
        foreach($votes as $vote)
        {
            $countAnswer = count($vote->answers);
            $answers = $vote->answers;
        }

        return view('admin.vote.list', compact(['votes', 'countAnswer']));
    }

    public function create()
    {
        return view('admin.vote.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'question1' => 'required',
            'question2' => 'required',
            'question3' => 'required',
            'question4' => 'required',
            'question5' => 'required',
        ]);

        $voteCreate = Vote::create([
           'title' => $request->title,
           'question1' => $request->question1,
           'question2' => $request->question2,
           'question3' => $request->question3,
           'question4' => $request->question4,
           'question5' => $request->question5,
           'question6' => $request->question6,
           'question7' => $request->question7,
           'question8' => $request->question8,
           'question9' => $request->question9,
           'question10' => $request->question10,
           'vote_for' => 'Student',
        ]);

        $student = \Spatie\Permission\Models\Role::findByName('Student');
        $users = $student->users;

        foreach ($users as $user) {
            $user_id = $user->id;
            $user->notify(new SendVoteNotification($voteCreate));
        }

        
        toast('نظرسنجی '. $request->input('title') .' با موفقیت ایجاد شد.', 'success');
        return redirect(route('admin.vote.index'));
    }

    public function edit(Vote $vote)
    {
        return view('admin.vote.edit', compact('vote'));
    }

    public function update(Vote $vote, Request $request)
    {
        $valid = $request->validate([
            'title' => 'required',
            'question1' => 'required',
            'question2' => 'required',
            'question3' => 'required',
            'question4' => 'required',
            'question5' => 'required',
            'question6' => 'required',
            'question7' => 'required',
            'question8' => 'required',
            'question9' => 'required',
            'question10' => 'required',
        ]);

        $vote->update($valid);
        toast('نظرسنجی '. $request->input('title') .' ویرایش شد.', 'info');
        return redirect(route('admin.vote.index'));

    }

    public function getAnswers($id)
    {
        $votes = Vote::whereId($id)->with(['answers'])->get();

        return view('admin.vote.answers-list', compact(['votes']));
    }

    public function delete($id)
    {
        Vote::find($id)->delete();
        toast('نظرسنجی حذف شد.', 'error');
        return back();
    }

}
