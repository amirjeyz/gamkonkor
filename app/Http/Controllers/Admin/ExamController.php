<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Lesson;
use App\Models\Option;
use App\Models\Question;
use App\Models\QuestionType;
use App\Models\Quiz;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class ExamController extends Controller
{

    public function index()
    {
        return view('admin.exam.index', [
            'exams' => Quiz::all()
        ]);
    }

    public function create()
    {
        $rolesWithUsers = Role::where('name', 'Student')->with('users')->get();

        foreach ($rolesWithUsers as $users) {
            foreach ($users->users as $user) {
                $userID = $user->id;
                $userIDArray[] = $userID;
            }
        }
        $myStudents = Student::whereIn('user_id', $userIDArray)->get() ?? null;

        foreach ($myStudents as $myStudent) {
            $myStudentsID[] = $myStudent->user_id;
        }

        $students = User::whereIn('id', $myStudentsID)->get();

        return view('admin.exam.create', [
            'types' => QuestionType::all() ?? null,
            'students' => $students ?? null
        ]);
    }

    public function store(Request $request)
    {
        $quiz = Quiz::create([
            'name' => $request->quiz_name,
            'expired_at' => $request->expired_at,
            'student_id' => $request->student_id,
            'creator_id' => Auth::user()->id,
            'end_time' => $request->end_time
        ]);
        $array = [];
        foreach ($request->q as $key => $value) {
            $array[]['question'] = $value;
        }

        foreach ($request->option1 as $key => $value) {
            $array[$key]['option1'] = $value;
        }
        foreach ($request->option2 as $key => $value) {
            $array[$key]['option2'] = $value;
        }
        foreach ($request->option3 as $key => $value) {
            $array[$key]['option3'] = $value;
        }
        foreach ($request->option3 as $key => $value) {
            $array[$key]['option3'] = $value;
        }
        foreach ($request->option4 as $key => $value) {
            $array[$key]['option4'] = $value;
        }
        foreach ($request->is_correct as $key => $value) {
            $array[$key]['is_correct'] = $value;
        }
        foreach ($request->type as $key => $value) {
            $array[$key]['type'] = $value;
        }

        foreach ($array as $key => $value) {
            $question = new Question();
            $question->quiz_id = $quiz->id;
            $question->body = $value['question'];
            $question->type_id = $value['type'];
            $question->save();

            $option = new Option();
            $option->question_id = $question->id;
            $option->option1 = $value['option1'];
            $option->option2 = $value['option2'];
            $option->option3 = $value['option3'];
            $option->option4 = $value['option4'];
            $option->is_correct = $value['is_correct'];
            $option->save();
        }

        toast('آزمون با موفقیت ایجاد شد.', 'success');
        return redirect(route('admin.exam.index'));
    }


    public function uploadImage(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('examImages'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('examImages/'.$fileName);
            $msg = 'عکس با موفقیت اضافه شد.';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function startExam(Request $request, $id)
    {
        Quiz::find($id)->update([
            'status' => 1
        ]);

        toast('آزمون با موفقیت شروع شد.', 'success');
        return back();
    }

    public function stopExam($id)
    {
        Quiz::find($id)->update([
            'status' => 2
        ]);

        toast('آزمون با موفقیت پایان یافت.', 'success');
        return back();
    }

    public function delete($id)
    {
        Quiz::find($id)->delete();

        toast('آزمون با حذف شد.', 'error');
        return back();
    }

    public function showLessons()
    {
        return view('admin.exam.lesson', [
            'lessons' => QuestionType::all()
        ]);
    }

    public function storeLesson(Request $request)
    {
        QuestionType::create([
            'name_fa' => $request->name_fa
        ]);

        Lesson::create([
            'name_fa' => $request->name_fa
        ]);

        toast('درس با موفقیت اضافه شد.', 'success');
        return back();

    }
}
