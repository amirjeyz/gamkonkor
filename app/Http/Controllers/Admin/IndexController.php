<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;
use Ghasedak\GhasedakApi;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin.index', [
            'ghasedak' => $this->ghasedakInfo(),
        ]);
    }

    public function ghasedakInfo()
    {
        if (env('APP_ENV') != 'local') {
            try {
                $api = new GhasedakApi('d0fdcc5cd7e9b2f1d59b8548f5145e1b29b18e1a06407330b1ed830265efeffb');
                $result = $api->AccountInfo();
            } catch (ApiException $e) {
                throw $e->errorMessage();
            } catch(HttpException $e) {
                throw $e->errorMessage();
            }

            return $result->items;
        }

        return null;
    }

}
