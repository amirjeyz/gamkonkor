<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Advisor;
use App\Models\AssignPackage;
use App\Models\Package;
use App\Models\Student;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Role;


class UserController extends Controller
{
    public static $roles = [
        'Student',
        'Advisor',
        'Admin',
        'Teacher'
    ];

    public function index()
    {
        $users = User::with('roles')->get();
        return view('admin.users.list', compact('users'));
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $conditions = [
            'type' => 'insert',
            'role' => null,
            'user' => null
        ];
        $request->validate($this->getRules($conditions));
        $user_id = User::create([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
        ])->id;
        $user = User::find($user_id);
        $user->assignRole($request->input('role'));
        toast(roleToPersian($request->input('role')) . ' با موفقیت ایجاد شد.', 'success');
        return redirect(route('admin.users.create.with.role', $user_id));
    }

    public function createWithRole(User $user)
    {
        return view('admin.users.create-with-role', compact('user'));
    }

    public function storeWithRole(User $user, Request $request)
    {
        $user_roles = array_intersect($user->getRoleNames()->toArray(), self::$roles);
        $conditions = [
            'type' => null,
            'role' => $user_roles[0],
            'user' => $user->id
        ];
        $request->validate($this->getRules($conditions));
        $this->insertOrUpdate($request, 'insert', $user->id, $user_roles[0]);
        toast('اطلاعات ' . roleToPersian($user_roles[0]) . ' با موفقیت ذخیره شد.', 'success');
        return redirect(route('admin.users.list'));
    }

    private function insertOrUpdate($request, $type, $user_id, $role): void
    {
        $data = null;
        $table = null;
        if ($role === 'Student') {
            $table = "students";
            $data = [
                'grade' => $request->input('grade'),
                'field' => $request->input('field'),
                'parent_phone' => $request->input('parent_phone'),
                'city' => $request->input('city'),
                'email' => $request->input('email')
            ];
            $file_path = null;
            if ($type === 'update') {
                $file_path = Student::where('user_id', $user_id)->pluck('file')->first();
            }
            if ($request->has('file')) {
                if (File::exists(public_path('storage\student\\' . $file_path))) {
                    File::delete(storage_path('storage\student\\' . $file_path));
                }
                $resize = Image::make($request->file('file'))->fit(50)->encode('jpg');
                $hash = md5($resize->__toString() . now());
                $path = public_path("storage\student\\{$hash}.jpg");
                File::put(
                    $path,
                    $resize
                );
                $data['file'] = $hash . '.jpg';
            }
        } elseif ($role === 'Advisor') {
            $table = "advisors";
            $file_path = null;
            if ($type === 'update') {
                $file_path = Advisor::where('user_id', $user_id)->pluck('experience_file')->first();
            }
            if ($request->has('experience_file')) {
                if ($type === 'update') {
                    $file_path = public_path('storage\advisors\\' . $file_path);
                    if (File::exists($file_path)) {
                        File::delete($file_path);
                    }
                }
                $file_path = $this->fileUploader('advisors', $request->file('experience_file'));
            }
            $data = [
                'user_id' => $user_id,
                'exam_rank' => $request->input('exam_rank'),
                'field' => $request->input('field'),
                'university' => $request->input('university'),
                'experience_year' => $request->input('experience_year'),
                'experience_text' => $request->input('experience_text'),
                'experience_file' => $file_path,
                'email' => $request->input('email'),
                'city' => $request->input('city'),
                'address' => $request->input('address'),
            ];
        }

        if ($type === 'insert') {
            $data['user_id'] = $user_id;
            $data['created_at'] = Carbon::now()->toDateTimeString();
            DB::table($table)->insert($data);
        } else {
            $data['updated_at'] = Carbon::now()->toDateTimeString();
            DB::table($table)->where('user_id', $user_id)->update($data);
        }
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user_roles = array_intersect($user->getRoleNames()->toArray(), self::$roles);
        $conditions = [
            'type' => 'update',
            'role' => $user_roles[0],
            'user' => $user->id
        ];
        $request->validate($this->getRules($conditions));
        $user->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone')
        ]);
        $this->insertOrUpdate($request, 'update', $user->id, $user_roles[0]);
        toast('اطلاعات جدید کاربر '. $request->input('name') .' با موفقیت ذخیره شد.', 'success');
        return redirect(route('admin.users.list'));
    }

    public function delete(User $user)
    {
        $user->delete();
        toast('حساب کاربری حذف شد.', 'error');
        return back();
    }

    private function getRules($conditions)
    {
        $rules = [];
        if ($conditions['type'] === "insert" || $conditions['type'] === "update") {
            $rules['name'] = ['required', 'min:5', 'max:150'];
        }
        if ($conditions['type'] === "insert") {
            $rules['role'] = ['in:Admin,Student,Advisor,Teacher'];
            $rules['phone'] = ['required', 'min:5', 'max:150', 'unique:users,phone'];
        } elseif ($conditions['type'] === "update") {
            $rules['phone'] = ['required', 'min:5', 'max:150', Rule::unique('users', 'phone')->ignore($conditions['user'])];
        }
        if (!($conditions['role'] === "Student" || $conditions['role'] === "Advisor")){
        return $rules;
    }
            $rules['field'] = ['required', 'string', 'min:5', 'max:150'];
            $rules['city'] = ['required', 'string', 'min:5', 'max:150'];
            if ($conditions['role'] === "Student") {
                $rules['parent_phone'] = ['required', 'min:5', 'max:150'];
                $rules['email'] = ['nullable', 'email'];
                $rules['grade'] = ['required', 'string', 'min:5', 'max:150'];
            } elseif ($conditions['role'] === "Advisor") {
                $rules['exam_rank'] = ['required', 'string', 'min:1', 'max:20'];
                $rules['university'] = ['required', 'string', 'min:5', 'max:250'];
                $rules['experience_year'] = ['required', 'integer', 'max:40'];
                $rules['experience_text'] = ['nullable', 'string', 'max:1500'];
                $rules['experience_file'] = ['mimes:doc,docx,pdf', 'max:5120'];
                $rules['email'] = ['required', 'string', 'min:5', 'max:250'];
                $rules['address'] = ['required', 'string', 'min:5', 'max:1500'];
            }

        return $rules;
    }

    private function fileUploader($path, $file): string
    {
        $filename = md5(Str::random(15) . time()) . '.' . $file->getClientOriginalExtension();
        File::put('storage/' . $path . '/' . $filename, file_get_contents($file));
        return $filename;
    }

    public function assignRole($id, Request $request)
    {
        $user = User::find($id);
        $user->assignRole($request->role);

        return redirect(route('admin.users.list'));
    }

    public function assignPerm($id, Request $request)
    {
        $user = User::find($id);
        $user->givePermissionTo($request->permission);

        return redirect(route('admin.users.list'));
    }

    public function getDownload(Request $request)
    {
        $file = public_path('storage\advisors\\' . $request->input('path'));
        if (File::exists($file)) {
            return response()->download($file);
        }
        return redirect()->back();
    }

    public function addCapacity(User $user, Request $request)
    {

        Advisor::where('user_id', $user->id)
                ->update([
                    'capacity' => $request->capacity
                ]);
        Alert::success('', 'ظرفیت مشاور مورد نظر افزایش یافت.');
        return back();
    }

    public function addAdvisorToStudent(Request $request, User $user)
    {
        // add Advisor ID to student
        $student = Student::whereUserId($user->id)->update([
            'advisor_id' => $request->advisor
        ]);

        $user->packages()->attach($request->package, [
            'expired_at' => Carbon::now()->addMonth(123)
        ]);

        // add Permission
        $user->givePermissionTo('academic-advice-pkg');

        Alert::success('', 'فرایند ثبت مشاور برای دانش آموز با موفقیت انجام شد.');
        return back();
    }

    public function assignLessonToAdvisor(Request $request, User $user)
    {
        $user->lessons()->detach();

        $user->lessons()->attach($request->lesson_id);

        Alert::success('', 'فرایند ثبت درس برای مدرس با موفقیت انجام شد.');
        return back();
    }
}
