<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdvisorSalary;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class BillsController extends Controller
{
    public function index()
    {
        $orders = Order::paginate(10);
        $salaries = AdvisorSalary::paginate(10);
        return view('admin.bills.index', compact('orders', 'salaries'));
    }

    public function createSalary()
    {
        $users = User::with('Advisor')->get();
        return view('admin.bills.createSalary', compact('users'));
    }

    public function storeSalary(Request $request)
    {

        $request->validate([
            'user_id'     => 'required',
            'title'       => 'required',
            'description' => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
            'price'       => 'required',
        ]);

        $salary = AdvisorSalary::create([
            'user_id'     => $request->user_id,
            'title'       => $request->title,
            'description' => $request->description,
            'start_date'  => $request->start_date,
            'end_date'    => $request->end_date,
            'price'       => $request->price,
        ]);

        return redirect(route('admin.bills.index'));
    }

    public function editSalary(AdvisorSalary $salary)
    {
        $users = User::with('Advisor')->get();

        return view('admin.bills.editSalary', compact('salary', 'users'));
    }

    public function updateSalary(AdvisorSalary $salary, Request $request)
    {
        $validate = $request->validate([
            'user_id'     => 'required',
            'title'       => 'required',
            'description' => 'required',
            'start_date'  => 'required',
            'end_date'    => 'required',
            'price'       => 'required',
        ]);

        $salary->update($validate);

        return redirect(route('admin.bills.index'));
    }

    public function deleteSalary(AdvisorSalary $salary)
    {
        $salary->delete();
        return back();
    }

    public function deleteOrder(Order $order)
    {
        $order->delete();
        return back();
    }
}
