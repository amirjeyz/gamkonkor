<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::with('episodeHa')->get();
        return view('admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required',
            'status' => 'required',
            'episodes' => 'required',
            'course_time' => 'required',
            'teacher_name' => 'required',
        ]);

        if ($request->has('cover')) {
            $file = $request->file('cover');
            $destinationPath = 'storage/course_cover/' . now()->year . '/' . now()->month . '/';
            $filename = $file->getClientOriginalName() . $file->getClientOriginalExtension();
            $file->move($destinationPath, $filename);
        }


        $course = Course::create([
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->input('content'),
            'price' => $request->price,
            'status' => $request->status,
            'episodes' => $request->episodes,
            'course_time' => $request->course_time,
            'user_id' => '1',
            'teacher_name' => $request->teacher_name,
            'cover' => $destinationPath . $filename,
        ]);

        toast('دوره‌ی '. $request->title .' با موفقیت ایجاد شد.', 'success');
        return redirect()->route('admin.courses.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.courses.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('admin.courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $validData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
            'price' => 'required',
            'status' => 'required',
            'episodes' => 'required',
            'course_time' => 'required',
            'teacher_name' => 'required',
        ]);

        $course->update($validData);
        toast('دوره‌ی ' . $request->input('title') . ' بروزرسانی شد.', 'info');
        return redirect(route('admin.courses.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        toast('دوره‌ی ' . $course->title . ' حذف شد.', 'error');
        $course->delete();
        return back();
    }
}
