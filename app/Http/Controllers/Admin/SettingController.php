<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SettingController extends Controller
{
    public function index()
    {
        return view('admin.settings.create');
    }

    public function setTitle(Request $request)
    {
        Setting::where('key', 'title')->update([
            'value' => $request->title
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setHeader(Request $request)
    {
        Setting::where('key', 'header')->update([
            'value' => $request->header
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setSubHeader(Request $request)
    {
        Setting::where('key', 'subheader')->update([
            'value' => $request->subheader
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setVerySubHeader(Request $request)
    {
        Setting::where('key', 'verysubheader')->update([
            'value' => $request->verysubheader
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setCommentOne(Request $request)
    {
        Setting::where('key', 'commenter-name1')->update([
            'value' => $request->commenter_name1
        ]);

        Setting::where('key', 'commenter-msg1')->update([
            'value' => $request->commenter_msg1
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setCommentTwo(Request $request)
    {
        Setting::where('key', 'commenter-name2')->update([
            'value' => $request->commenter_name2
        ]);

        Setting::where('key', 'commenter-msg2')->update([
            'value' => $request->commenter_msg2
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setCommentThree(Request $request)
    {
        Setting::where('key', 'commenter-name3')->update([
            'value' => $request->commenter_name3
        ]);

        Setting::where('key', 'commenter-msg3')->update([
            'value' => $request->commenter_msg3
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setContact(Request $request)
    {
        Setting::where('key', 'contact-text')->update([
            'value' => $request->contact_text
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }

    public function setAboutUs(Request $request)
    {
        Setting::where('key', 'about-us')->update([
            'value' => $request->about_us
        ]);

        Alert::success('', 'تغییرات با موفقیت اعمال شد!');
        return back();
    }
}
