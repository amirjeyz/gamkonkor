<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PackagesController extends Controller
{
    public function index()
    {
        $packages = Package::all();
        return view('admin.packages.index', compact('packages'));
    }

    public function create()
    {
        return view('admin.packages.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'expire_time' => 'required',
            'description' => 'required',
            'price' => 'required',
            'permissions' => 'required',
        ]);

        $package = new Package();
        $package->title = $request->title;
        $package->expire_time = $request->expire_time;
        $package->description = $request->description;
        $package->price = $request->price;
        $package->save();

        $package->permissions()->attach($request->permissions);

        Alert::success('', 'پکیج با موفقیت ایجاد شد.');
        return redirect(route('admin.packages.index'));
    }

    public function edit(Package $package)
    {
        return view('admin.packages.edit', compact('package'));
    }

    public function update(Request $request, Package $package)
    {
        $validate = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'cover' => 'required',
            'price' => 'required',
        ]);

        $package->update($validate);

        return redirect(route('admin.packages.index'));
    }

    public function delete($id)
    {
        Package::find($id)->delete();

        return back();
    }
}
