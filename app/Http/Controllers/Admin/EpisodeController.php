<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Episode;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EpisodeController extends Controller
{
    public function showAddEpisode($id) {
        $course = Course::where('id', $id)->first();
        return view('admin.courses.videos', compact('course'));
    }

    public function storeAddEpisode(Request $request, $id)
    {
        $validate = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'teacher_name' => 'required',
            'price' => 'required',
            'video_link' => 'required'
        ]);

        $episode = Episode::create([
            'course_id' => $id,
            'title' => $request->title,
            'description' => $request->description,
            'teacher_name' => $request->teacher_name,
            'price' => $request->price,
            'user_id' => '1',
            'cover' => null,
        ]);
        if ($request->has('video_link')){
            $file = $request->file('video_link');
            $name = time().$file->getClientOriginalName();
            $filePath = 'episodes/' . $name;
            $s = Storage::disk('s3')->put($filePath, file_get_contents($file));
            $episode->update(['video_link' => Storage::disk('s3')->url($filePath)]);
        }

        toast('ویدیو با موفقیت ذخیره شد.', 'success');
        return redirect(route('admin.courses.index'));
    }

    public function deleteEpisode($id)
    {
        $episode = Episode::find($id);
        $episode->delete();
        toast('ویدیو حذف شد.', 'error');
        return redirect(route('admin.courses.index'));
    }

    public function edit(Episode $episode)
    {
        $getCourse = $episode->with('course')->get();
        foreach ($getCourse as $item) {
            $courseName = $item->course->title;
        }
        return view('admin.courses.edit-video', compact('courseName', 'episode'));
    }

    public function update(Request $request, Episode $episode)
    {
        $validate = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'teacher_name' => 'required',
            'price' => 'required',
            'video_link' => 'required'
        ]);

        $episode->update($validate);
        toast('ویدیو بروزرسانی شد.', 'info');
        return redirect(route('admin.courses.index'));
    }
}
