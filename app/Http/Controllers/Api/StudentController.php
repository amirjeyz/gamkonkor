<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Lesson;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function getTeachers(Lesson $lesson)
    {
        $users = $lesson->users;
        foreach ($users as $user) {
            $teachers[] = [
                'name' => $user->name,
                'id' => $user->id
            ];
        }

         return response()->json($teachers);
    }
}
