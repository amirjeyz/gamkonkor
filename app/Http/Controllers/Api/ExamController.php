<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Option;
use App\Models\Question;
use App\Models\Quiz;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
    public function getQuestions($quiz)
    {
        $questions = Question::where('quiz_id', $quiz)->get();

        return response()->json([
            'message' => 'success',
            'data' => $questions
        ], 200);
    }

    public function remainingTime($quiz)
    {
        $quiz = Quiz::find($quiz);
        $time = Carbon::now()->addMinutes($quiz->end_time)->timestamp;

        return response()->json([
            'message' => 'success',
            'data' => $time
        ], 200);
    }

    public function storeAnswers(Request $request, $quiz)
    {
        foreach ($request->answers as $value) {
            $answer = new Answer();
            $answer->question_id = $value['question_id'];
            $answer->answer = $value['answer'];
            $answer->user_id = Auth::user()->id;
            $answer->quiz_id = $quiz;
            $answer->type_id = $value['type_id'];

            $a = Option::where('question_id', $value['question_id'])
                        ->where('is_correct', $value['answer'])
                        ->first();

            if ($a) {
                $answer->is_correct = 1;
            } else {
                $answer->is_correct = 0;
            }

            $answer->save();
        }

        return response()->json([
            'message' => 'success'
        ], 201);
    }

}
