<?php

namespace App\Http\Controllers;

use App\Models\Advisor;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class AdvisorController extends Controller
{
    public function index()
    {
        return view('advisor.index', [
            'user' => Auth::user()
        ]);

    }

    public function studentsList(User $user)
    {
        // get students list
    }

    public function billsList(User $user)
    {
        // get bills list
    }

    public function studentProfile(User $user)
    {
        return view('advisor.student-profile', compact('user'));
    }

    public function profile()
    {
        return view('advisor.profile.profile', [
            'user' => Auth::user()
        ]);
    }

    public function edit()
    {
        return view('advisor.profile.edit', [
            'user' => Auth::user()
        ]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|string|min:5|max:150',
            'phone' => 'required|min:5|max:20|' . Rule::unique('users', 'phone')->ignore($user->id),
            'exam_rank' => 'required|string|min:1|max:20',
            'field' => 'required|string|min:5|max:150',
            'university' => 'required|string|min:5|max:150',
            'experience_year' => 'required|integer|max:40',
            'city' => 'required|string|min:5|max:150',
            'email' => 'nullable|email|max:250',
            'experience_text' => 'nullable|string|max:1500',
            'experience_file' => 'mimes:doc,docx,pdf|max:5120',
            'address' => 'required|string|min:5|max:1500',
        ]);
        $data = [
            'exam_rank' => $request->input('exam_rank'),
            'field' => $request->input('field'),
            'university' => $request->input('university'),
            'experience_year' => $request->input('experience_year'),
            'city' => $request->input('city'),
            'email' => $request->input('email'),
            'experience_text' => $request->input('experience_text'),
            'address' => $request->input('address'),
        ];
        if (!$request->has('experience_file')){
        $user->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
        ]);
        Advisor::where('user_id', $user->id)->update($data);
        toast('اطلاعات حساب کاربری شما با موفقیت بروزرسانی شد.', 'success');
        return redirect(route('advisor.profile'));
    }
            if (File::exists(public_path('storage\advisors\\' . $user->advisor->experience_file))) {
                File::delete(public_path('storage\advisors\\' . $user->advisor->experience_file));
            }
            $filename = md5(Str::random(15) . time()) . '.' . $request->file('experience_file')->getClientOriginalExtension();
            File::put('storage/advisors/' . $filename, file_get_contents($request->file('experience_file')));
            $data['experience_file'] = $filename;

        $user->update([
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
        ]);
        Advisor::where('user_id', $user->id)->update($data);
        toast('اطلاعات حساب کاربری شما با موفقیت بروزرسانی شد.', 'success');
        return redirect(route('advisor.profile'));
    }

    public function newPassword()
    {
        return view('advisor.profile.new-password', [
            'user' => Auth::user()
        ]);
    }

    public function updatePassword(Request $request, User $user)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
        $user->update([
            'password' => Hash::make($request->input('password'))
        ]);
        toast('رمز عبور شما بروزرسانی شد.', 'info');
        return redirect(route('advisor.profile'));
    }
}
