<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class RegisterController extends Controller
{
    public function showRegister()
    {
        return view('auth.register');
    }


    public function storeRegister(Request $request)
    {
        // regex:/^09(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}$/
        // return $request->all();
        $validData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users,phone']
        ]);

        $user = User::create($validData);
        $user->assignRole('Student');

        Alert::success('', 'ثبت نام شما با موفقیت انجام شد.')->autoClose(5000)->showConfirmButton('خیلی خب!');
        return redirect(route('auth.login'));
    }
}
