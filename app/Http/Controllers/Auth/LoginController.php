<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ActiveCode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ghasedak\GhasedakApi;

class LoginController extends Controller
{
    public function showLogin()
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole('Admin')) {
                return redirect('/admin');
            } elseif (Auth::user()->hasRole('Advisor')) {
                return redirect('/advisor');
            } elseif (Auth::user()->hasRole('Student')) {
                return redirect('/student');
            }
        }

        return view('auth.login');
    }


    public function storeLogin(Request $request)
    {
        $validData = $request->validate([
           'phone' => ['required', 'exists:users,phone']
        ], [
            'phone.required' => 'شماره تماس را الزامی است.',
            'phone.exists' => 'کاربری با این شماره تماس وجود ندارد.',
            'phone.regex' => 'شماره تماس اشتباه میباشد.'
        ]);

        $user = User::wherePhone($validData['phone'])->first();

        $request->session()->flash('auth', [
           'user_id' => $user->id,
           'remember' => $request->has('remember'),
        ]);

        $code = ActiveCode::generateCode($user);

        //TODO:
        $api = new GhasedakApi('d0fdcc5cd7e9b2f1d59b8548f5145e1b29b18e1a06407330b1ed830265efeffb');
        $api->Verify($user->phone, 1, 'gamauth', $code);

        return redirect(route('auth.phone.token'));
    }
}
