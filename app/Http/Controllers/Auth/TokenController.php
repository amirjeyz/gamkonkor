<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ActiveCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TokenController extends Controller
{
    public function showToken(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->hasRole('Admin')) {
                return redirect('/admin');
            } elseif (Auth::user()->hasRole('Advisor')) {
                return redirect('/advisor');
            } else {
                return redirect('/student');
            }
        }


        if (! $request->session()->has('auth')) {
            return redirect(route('auth.login'));
        }

        $request->session()->reflash();

        return view('auth.verify');
    }

    public function storeToken(Request $request)
    {
        $request->validate([
           'token' => 'required'
        ]);

        if (! $request->session()->has('auth')) {
            return redirect(route('auth.login'));
        }

        $user = \App\Models\User::findOrFail($request->session()->get('auth.user_id'));

        $status = ActiveCode::verifyCode($request->token, $user);

        if (! $status) {
            return redirect(route('auth.login'));
        }

        if (Auth::loginUsingId($user->id, $request->session()->get('auth.remember'))){

            $user->activeCode()->delete();

            if (Auth::user()->hasRole('Admin')) {
                return redirect('/admin');
            } elseif (Auth::user()->hasRole('Advisor')) {
                return redirect('/advisor');
            } else {
                return redirect('/student');
            }
        }

        return redirect(route('auth.login'));
    }
}
