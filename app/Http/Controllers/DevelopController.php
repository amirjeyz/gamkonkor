<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Permission;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DevelopController extends Controller
{
    public function dev(Request $request)
    {

        /*
         * Create package
         */

//        $createPackage = Package::create([
//            'title' => $request->title,
//            'expire_time' => $request->expire_time,
//            'description' => $request->description,
//        ]);

        /*
         * Assign user to package
         */

//        $package = Package::find(1);
//        $user = User::find(7);
//        $assignUserToPackage = $package->users()->attach($user->id, [
//            'expired_at' => Carbon::now()->addMonth(15)
//        ]);

        /*
         * Assign package to permission
         */
        $package = Package::find(1);
        $permissions = Permission::whereIn('id', [1, 2, 3])->get();
        $permissionIDs = [];
        foreach ($permissions as $permission) {
            $permissionIDs[] = $permission->id;
        }

        $package->permissions()->attach($permissionIDs);

    }
}
