<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoteAnswer extends Model
{
    protected $fillable = [
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer5',
        'answer6',
        'answer7',
        'answer8',
        'answer9',
        'answer10',
        'sum',
        'vote_id',
        'user_id',
        'advisor_id',
    ];

    public function vote()
    {
        return $this->belongsTo(Vote::class, 'vote_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function adviser()
    {
        return $this->belongsTo(User::class, 'advisor_id', 'id');
    }
}
