<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'price', 'type', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class,  'course_order', 'order_id','course_id');
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'order_package', 'order_id', 'package_id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }
}
