<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'question_id',
        'user_id',
        'answer',
        'is_correct',
        'quiz_id',
        'type_id'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
