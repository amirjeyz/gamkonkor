<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignPackage extends Model
{
    protected $fillable = [
        'user_id',
        'package_id',
        'payment_time',
        'expired_at',
        'permission'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
