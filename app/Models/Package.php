<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class Package extends Model
{
    protected $fillable = [
        'title',
        'expire_time',
        'description',
        'price'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'package_user', 'package_id', 'user_id')
                    ->withPivot(
                        'expired_at'
                    );
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'package_permission', 'package_id', 'permission_id');
    }
}
