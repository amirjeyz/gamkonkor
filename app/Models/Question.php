<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $with = ['option'];

    protected $fillable = [
        'quiz_id',
        'body',
        'type_id'
    ];

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function option()
    {
        return $this->hasOne(Option::class);
    }
}
