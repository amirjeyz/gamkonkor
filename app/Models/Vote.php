<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Vote extends Model
{
    use Notifiable;

    protected $fillable = [
        'title',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5',
        'question6',
        'question7',
        'question8',
        'question9',
        'question10',
        'vote_for',
    ];

    public function answers()
    {
        return $this->hasMany(VoteAnswer::class);
    }
}
