<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvisorSalary extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'start_date',
        'end_date',
        'price'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
