<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $fillable = [
        'user_id', 'exam_rank', 'field', 'experience_text', 'experience_file', 'university', 'experience_year', 'email', 'city', 'address'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public static function getAdvisorsForReservation()
    {
        return Advisor::where('capacity', '!=', 0)
                    ->with('user')
                    ->orderBy('capacity', 'ASC')
                    ->get();
    }

}
