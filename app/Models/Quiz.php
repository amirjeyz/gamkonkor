<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Question;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Quiz extends Model
{
    protected $fillable = [
        'name',
        'expired_at',
        'status',
        'student_id',
        'creator_id',
        'end_time'
    ];

    public static function countUserAnswer($quiz)
    {
        $question = Question::where('quiz_id', $quiz)->first();
        if ($question) {
            return Answer::where('question_id', $question['id'])->groupBy('user_id')->count();
        }
        return 0;
    }

    public static function userHasAnswer($id)
    {
        $quiz = Quiz::where('student_id', Auth::user()->id)
                    ->where('id', $id)
                    ->with('questions')
                    ->first();
        $hasAnswers = null;
        foreach ($quiz->questions as $item) {
            $hasAnswers = $item->answers;
        }

        return $hasAnswers;
    }


    public function generateResult()
    {
        $quiz = Quiz::find($this->id);

        $questionsSeparationTypesQuery = "
        SELECT count(answers.id) as cnt, type_id, question_types.name_fa FROM answers
        left join question_types on answers.type_id = question_types.id
        where answers.quiz_id = {$quiz->id}
        group by type_id
        ";

        $questionsSeparationTypes = DB::select(DB::raw($questionsSeparationTypesQuery));

        $karnameArray = [];

        foreach ($questionsSeparationTypes as $question) {
            $correctAnswer = Answer::where('type_id', $question->type_id)
                ->where('is_correct', 1)
                ->count();

            $darsad = ( (($correctAnswer * 3) - ($question->cnt - $correctAnswer)) / ($question->cnt * 3) ) * 100 ;
            $karnameArray[] = [
                'total' => $question->cnt,
                'name' => $question->name_fa,
                'type' => $question->type_id,
                'darsad' => round($darsad, 2)
            ];
        }

        return $karnameArray;
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }
}
