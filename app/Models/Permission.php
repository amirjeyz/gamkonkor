<?php

namespace App\Models;

use App\Models\Package;

class Permission extends \Spatie\Permission\Models\Permission
{
    public function packages()
    {
        return $this->belongsToMany(Package::class, 'package_permission', 'permission_id', 'package_id');
    }

    public function getPersianName()
    {
        switch ($this->name) {
            case 'exam':
                $fa = 'آزمون';
                break;
            case 'academic-advice-pkg':
                $fa = 'مشاوره تحصیلی';
                break;
            case 'psychology-pkg':
                $fa = 'روانشناسی';
                break;
            case 'debugging-pkg':
                $fa = 'رفع اشکال';
                break;
        }
        return $fa;
    }
}
