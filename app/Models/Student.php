<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Student extends Model
{
    protected $fillable = ['user_id', 'advisor_id', 'expire', 'grade', 'field', 'email', 'city', 'parent_phone', 'file', 'psychologist_id', 'teacher_id'];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getRouteKeyName(): string
    {
        return "id";
    }

    public function advisor()
    {
        return $this->hasOne(Advisor::class);
    }
}
