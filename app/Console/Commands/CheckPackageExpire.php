<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use App\Notifications\Panel\PackageExpireNotification;
use App\Notifications\SendVoteNotification;
use App\Notifications\Sms\PackageExpireNotification as SmsPackageExpireNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class CheckPackageExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'service:check-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check package expire time for students';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::with('student', 'permissions')->get();

        foreach ($users as $user) {
            foreach ($user->assignPackages as $package) {
                $now  = Carbon::make($package->payment_time);
                $end  = $package->expired_at;
                // show difference in days between now and end dates
                $daysLeft = $now->diffInDays($end);
                if ($daysLeft < 5) {
                   foreach ($user->permissions as $p) {
                       if ($p->name == $package->permission) {

                           $order = Order::create([
                               'user_id' => '3',
                               'price' => $package->package->price,
                               'type' => '1',
                               'status' => 'unpaid'
                           ]);

                           $order->packages()->attach($package->package->id);

                           Notification::send($user, new PackageExpireNotification($package->package));
                           Notification::send($user, new SmsPackageExpireNotification($package->package->title, $user->name, $user->phone));

                           echo $user->name . ' = its done';
                       } else {
                           echo 'no';
                       }
                   }
                } else {
                    echo 'nafresteshh              ';
                }

            }
        }
    }
}
