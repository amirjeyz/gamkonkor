<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web/home.php'));

        Route::middleware(['web', 'auth', 'auth.admin'])
            ->namespace($this->namespace)
            ->prefix('admin')
            ->name('admin.')
            ->group(base_path('routes/web/admin.php'));

        Route::middleware(['web'])
            ->namespace($this->namespace)
            ->prefix('teacher')
            ->group(base_path('routes/web/teacher.php'));

        Route::middleware(['web', 'auth', 'auth.student'])
            ->namespace($this->namespace)
            ->prefix('student')
            ->name('student.')
            ->group(base_path('routes/web/student.php'));

        Route::middleware(['web', 'auth', 'auth.advisor'])
            ->namespace($this->namespace)
            ->prefix('advisor')
            ->group(base_path('routes/web/advisor.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
