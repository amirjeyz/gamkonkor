<?php

namespace App\Notifications\Sms;

use App\Notifications\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PackageExpireNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($package, $user, $phone)
    {
        $this->user = $user;
        $this->package = $package;
        $this->phone = $phone;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toGhasedakSms()
    {
        $text = $this->user . ' عزیز،' . "\n" . 'مهلت استفاده از پکیج ' . $this->package . 'رو به اتمام است. برای تمدید به پنل کاربری خود مراجعه کنید.' . "\n\n" . 'گام کنکور' . "\n" . 'https://gamkonkor.ir/';
        return [
            'text' => $text,
            'number' => '09359897943'
        ];
    }
}
