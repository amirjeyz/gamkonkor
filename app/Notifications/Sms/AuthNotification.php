<?php

namespace App\Notifications\Sms;

use App\Notifications\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AuthNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($code, $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toGhasedakSms()
    {
        return [
            'text' => "code: " . $this->code . "\nبا استفاده از کد بالا وارد شوید. \nگام کنکور\nhttp://gamkonkor.ir",
            'number' => $this->phoneNumber
        ];
    }
}
