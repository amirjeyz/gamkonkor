<?php

namespace App\Notifications\Channels;

use Ghasedak\Exceptions\ApiException;
use Ghasedak\Exceptions\HttpException;
use Ghasedak\GhasedakApi;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    public function send($notifiable, Notification $notification)
    {
        if (! method_exists($notification, 'toGhasedakSms')) {
            throw new \Exception("toGhasedakSMS Not found");
        }

        $message = $notification->toGhasedakSMS()['text'];
        $receptor = $notification->toGhasedakSMS()['number'];


        try {
            $lineNumber = "";
            $api = new GhasedakApi('d0fdcc5cd7e9b2f1d59b8548f5145e1b29b18e1a06407330b1ed830265efeffb');
            $api->sendSimple($receptor, $message, $lineNumber);
        } catch (ApiException $e) {
            throw $e->errorMessage();
        } catch(HttpException $e) {
            throw $e->errorMessage();
        }

    }


}
