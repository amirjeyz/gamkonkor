<?php

namespace App\Helpers;

class EnToFa
{
    public static function convert($number)
    {
        $en = array("0","1","2","3","4","5","6","7","8","9");
        $fa = array("۰","۱","۲","۳","۴","۵","۶","۷","۸","۹");
        $numberFa = str_replace($en, $fa, $number);
        return $numberFa;
    }
}
