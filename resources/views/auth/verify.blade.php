<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ثبت نام پرتال کاربری</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="{{asset('front-files/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('front-files/css/font-awesome.min.css')}}">
</head>

<body>
<div class="main h-screen  flex justify-center items-center">
    <div class="login-page relative  shadow-2xl  grid md:grid-cols-2">
        <div class="left flex p-16 pb-24 pt-24  justify-center items-center">
            <div class="content  flex flex-col items-center">
                <img src="{{asset('front-files/img/alexshatov_lock.png')}}" alt="">
                <h1 class="font-black text-white text-center mb-4">سلام,دوست عزیز!</h1>
                <p class="text-white">برای استفاده از امکانات سایت نیاز به یک حساب کاربری دارید اگر تاکنون حسابی داشته اید.همین حالا وارد
                    شوید</p>
            </div>
        </div>
        <div class="right text-black  ">
            <div class="content p-16 pb-24 pt-24 ">
                <h1 class="font-black text-center">ثبت نام به حساب کاربری</h1>
                <form class=" flex justify-center items-center flex-col mt-8" method="post" action="{{route('auth.phone.storeToken')}}">
                    @csrf
                    <input class="rounded-md bg-gray-100 w-64 p-2" placeholder="کد ارسال شده" type="text" name="token">

                    <div class="button flex justify-center">
                        <button class="bg-green-400 text-white rounded-md mt-6 p-2">تایید و ثبت نام</button>
                    </div>
                    <div class="checkbox flex flex-col">
                        <div class="right">
                            <input type="checkbox" class="mt-6">
                            <span class="pr-2">مرا به خاطر بسپار</span>
                        </div>
                        <div class="left">
                            <span class="pt-4 mx-auto" id="send_code_timer">
                                2:00
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="home absolute rounded-full p-2 left-4 bottom-4">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                     x="0px" y="0px" viewBox="0 0 512 512"
                     style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M506.188,236.413L297.798,26.65c-0.267-0.27-0.544-0.532-0.826-0.786c-22.755-20.431-57.14-20.504-79.982-0.169    c-0.284,0.253-0.56,0.514-0.829,0.782L5.872,236.352c-7.818,7.804-7.831,20.467-0.028,28.285    c7.804,7.818,20.467,7.83,28.284,0.028L50,248.824v172.684c0,44.112,35.888,80,80,80h72c11.046,0,20-8.954,20-20v-163h70v163    c0,11.046,8.954,20,20,20h70c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20    c0,22.056-17.944,40-40,40h-50v-163c0-11.046-8.954-20-20-20H202c-11.046,0-20,8.954-20,20v163h-52c-22.056,0-40-17.944-40-40    v-212c0-0.2-0.003-0.399-0.009-0.597L243.946,55.26c7.493-6.363,18.483-6.339,25.947,0.055L422,208.425v113.083    c0,11.046,8.954,20,20,20c11.046,0,20-8.954,20-20v-72.82l15.812,15.917c3.909,3.935,9.047,5.904,14.188,5.904    c5.097,0,10.195-1.937,14.096-5.812C513.932,256.912,513.974,244.249,506.188,236.413z"/>
                            </g>
                        </g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                    <g></g>
                </svg>
            </div>
        </div>
    </div>
</div>
@include('sweetalert::alert')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        let timer2 = "2:00"
        let interval = setInterval(function () {
            // Get today's date and time
            let timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            let minutes = parseInt(timer[0], 10);
            let seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0) clearInterval(interval);
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            if (minutes < 0) {
                clearInterval(interval);
                $('#send_code_timer').attr('id', 'send_code_btn').addClass('underline').html('<a href="{{route('auth.login')}}" style="color: inherit">تلاش مجدد</a>');
            } else {
                $('#send_code_timer').html(minutes + ':' + seconds);
                timer2 = minutes + ':' + seconds;
            }
        }, 1000);
    })
</script>
</body>

</html>
