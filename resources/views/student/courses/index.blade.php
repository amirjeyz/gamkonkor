@extends('student.layouts.main')

@section('title', 'لیست دوره‌ها')

@section('css')
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

<style>
</style>
@endsection

@section('content')

<div class="min-h-screen bg-white-100">
    <div class="min-h-screen max-w-5xl mx-auto place-content-start justify-start justify-items-start grid md:grid-cols-2 lg:grid-cols-3 gap-x-14 gap-y-5">
        @foreach ($checkOrders as $order)
            @foreach ($order->courses as $course)
                <div class="bg-white shadow-lg rounded-xl overflow-hidden max-w-xs order-5 lg:order-none">
                    <div>
                        <img src="{{asset('front-files/img/google-ads-bid-strategies-cover (image).png')}}" alt="Photography" class="w-full h-40 sm:h-48 object-cover">
                    </div>
                    <div class="py-5 px-6 sm:px-8">
                        <h2 class="text-xl sm:text-2xl text-gray-800 font-semibold mb-3">دوره {{$course->title}}</h2>
                        <p class="text-gray-500 leading-relaxed">{{\Illuminate\Support\Str::limit($course->description, 150, '...')}}</p>
                        <a href="{{route('courses.single‍', $course->id)}}" class="rounded bg-green-500 px-4 py-2 text-white w-100">مشاهده دوره</a>
                    </div>
                </div>
            @endforeach
        @endforeach

  </div>
</div>

@endsection
