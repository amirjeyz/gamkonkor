<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Hey dev.. u should add a meta tag of csrf token here! -->
    <title>آزمون</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/font-awesome.min.css">
</head>

<body>
<section class="bg-gray-100 flex justify-center items-center min-h-screen">
    <div class="content w-full m-12 p-12 bg-white shadow-lg rounded-lg exam-container">
        <div class="exam-questions-container">
            <div class="loading">
                <span class="justify-center flex">
                    <svg class="animate-spin -ml-1 mr-3 h-5 w-5 text-black" viewBox="0 0 24 24">
                        <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor"
                                stroke-width="4"></circle>
                        <path class="opacity-75" fill="currentColor"
                              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                    </svg>
                    <span class="text-gray-700 mr-3">در حال بارگزاری سوالات</span>
                </span>
            </div>
        </div>
        <div class="mt-12 flex justify-between items-center">
            <div>
                <span>مدت زمان باقی مانده:</span>
                <span class="font-black text-lg countdown"></span>
            </div>
            <div>
                <input type="hidden" name="exam_id" value="{{request()->id}}">
                <button class="bg-red-700 hover:bg-red-600 shadow-lg rounded-lg p-4 text-white" id="submit_exam">
                    ثبت و ارسال پاسخ ها
                </button>
            </div>
        </div>
    </div>
</section>
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    $(document).ready(function () {
        const state = {
            "answers": []
        };
        $(".exam-questions-container").on('click', 'input:checkbox', function () {
            let group = "input:checkbox[name='" + $(this).prop("name") + "']";
            $(group).not(this).prop("checked", false);
            for (let i = 0; i < state.answers.length; i++) {
                if (state.answers[i].question_id === $(this).prop("name")) {
                    $(this).is(':checked') ?
                        state.answers[i].answer = $(this).attr("option_number") :
                        state.answers[i].answer = "0";
                }
            }
        });
        const exam_id = $('input[name="exam_id"]').val()
        $.ajax({
            url: "https://gamkonkor.ir/api/exam/" + exam_id + "/questions",
            method: "GET",
        }).done(function (data) {
            data.data.map((question) => {
                let quiz_html = `<div class="mb-10 pb-10" style="border-bottom: 1px solid #e0e0e0;">
                    <div>
                        <h1 class="font-black text-lg mb-6 text-black">${question.body}</h1>
                    </div>
                    <div class="grid grid-cols-4 gap-6">
                        <label class="checkbox make_radio">
                            <input type="checkbox" name="${question.option.question_id}" option_number="1">
                            <span>${question.option.option1}</span>
                        </label>
                        <label class="checkbox make_radio">
                            <input type="checkbox" name="${question.option.question_id}" option_number="2">
                            <span>${question.option.option2}</span>
                        </label>
                        <label class="checkbox make_radio">
                            <input type="checkbox" name="${question.option.question_id}" option_number="3">
                            <span>${question.option.option3}</span>
                        </label>
                        <label class="checkbox make_radio">
                            <input type="checkbox" name="${question.option.question_id}" option_number="4">
                            <span>${question.option.option4}</span>
                        </label>
                    </div>
                </div>`
                $(".loading").hide();
                $(".exam-questions-container").append(quiz_html);
                state.answers.push({
                    "question_id": `${question.option.question_id}`,
                    "answer": "0",
                    "type_id": `${question.type_id}`
                })
            })
        });
        // Get exam expires time
        $.get("https://gamkonkor.ir/api/exam/" + exam_id + "/time", function (data) {

            function toMMSS(s) {
                let minutes = Math.floor(s / 60).toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
                let seconds = (s - minutes * 60).toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
                return minutes + ":" + seconds;
            }

            const currentTimestamp = Math.floor(Date.now() / 1000);
            const endTimestamp = data.data;

            var count = new Date(endTimestamp).getTime() - new Date(currentTimestamp).getTime();
            var counter = setInterval(timer, 1000);

            function timer() {

                if (parseInt(count) <= 0) {
                    clearInterval(counter);
                    return;
                }
                var temp = toMMSS(Number(count));
                count = (parseInt(count) - 1).toString();
                $('.countdown').html(temp);
            }
        });
        $("#submit_exam").on('click', function () {
            console.log(state)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.post("https://gamkonkor.ir/webapi/exam/" + exam_id + "/store",
                state, function (data) {
                    setTimeout(function() {
                        window.location = 'https://gamkonkor.ir/student/exam/';
                    }, 5000);
                    let timerInterval
                    Swal.fire({
                        title: 'آزمون با موفقیت ثبت شد!',
                        html: 'تا <b></b> دیگر به پنل کاربری خود منتقل خواهید شد.',
                        timer: 5000,
                        icon: 'success',
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    }).then((result) => {
                        /* Read more about handling dismissals below */
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                    })
                })
        })
    })
</script>
</body>
</html>