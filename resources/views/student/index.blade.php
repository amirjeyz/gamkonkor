@extends('student.layouts.main')

@section('title', 'داشبورد')


@section('content')
    <div class="row">


        <!-- Advisor information -->
        @can('academic-advice-pkg')
        <div class="col-xlg-4 col-lg-6 col-12 mb-30">
            <div class="box">
                <div class="box-head">
                    <h4 class="title">اطلاعات مشاور من</h4>
                </div>
                <div class="box-body">
                    <div class="d-flex justify-content-center align-items-center" style="flex-direction: column;">
                        <div class="avatar avatar-xxl mr-10 mb-10">
                            <img src="assets/images/avatar/avatar-1.jpg" alt="">
                        </div>
                        <h4>{{$advisor->name ?? ''}}</h4>
                        <hr>
                    </div>
                    <div class="d-flex justify-content-center">
                        <h5>
                            رشته تحصیلی:
                        </h5>
                        <p>{{$advisor->advisor->field ?? ''}}</p>
                    </div>
                    <div class="d-flex justify-content-center">
                        <h5>
                                رتبه کنکور:
                            </h5>
                        <p>{{$advisor->advisor->exam_rank ?? ''}}</p>
                    </div>
                    <div class="d-flex justify-content-center">
                        <h5>
                            سابقه فعالیت:
                        </h5>
                        <p>{{$advisor->advisor->experience_year ?? ''}} سال</p>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="https://api.whatsapp.com/send?phone={{$advisor->phone ?? ''}}" class="btn btn-sm btn-success">
                            <i class="fa fa-whatsapp"></i>
                            ارسال پیام در WhatsApp
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Advisor information [END] -->
        @endcan
        <!-- My Packages Status -->
        <div class="col-xlg-4 col-lg-6 col-12 mb-30">
            <div class="box">
                <div class="box-head">
                    <h4 class="title">وضعیت پکیج‌های من</h4>
                </div>
                <div class="box-body">
                    @foreach($packagesStatus as $package)
                        <div class="row d-flex justify-content-between">
                            <h4>{{$package['packageTitle']}}</h4>
                            <h4>{{$package['days']}} روز‌ باقی مانده</h4>
                        </div>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- My Packages Status -->


    </div>
@endsection

