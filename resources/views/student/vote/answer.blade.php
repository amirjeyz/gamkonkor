@extends('student.layouts.main')

@section('title', 'نظرسنجی')


@section('content')
    <div class="row">

        <div class="col-12">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">پاسخ به نظرسنجی {{$vote->title}}</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('student.vote.submit', $vote->id)}}" method="POST">
                        @csrf
                        <div class="row mbn-15">

                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question1}}</h6>
                                <input type="number" name="answer1" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question2}}</h6>
                                <input type="number" name="answer2" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question3}}</h6>
                                <input type="number" name="answer3" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question4}}</h6>
                                <input type="number" name="answer4" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question5}}</h6>
                                <input type="number" name="answer5" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question6}}</h6>
                                <input type="number" name="answer6" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question7}}</h6>
                                <input type="number" name="answer7" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question8}}</h6>
                                <input type="number" name="answer8" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question9}}</h6>
                                <input type="number" name="answer9" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <h6 class="mb-15">{{$vote->question10}}</h6>
                                <input type="number" name="answer10" max="10" min="0" class="form-control">
                            </div>
                            <div class="col-12 mb-15">
                                <button type="submit" class="btn btn-success" style="width: 100%;">تایید</button>

                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')

@endsection
