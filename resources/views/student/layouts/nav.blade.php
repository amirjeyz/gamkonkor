<!-- Header Section Start -->
<div class="header-section">
    <div class="container-fluid">
        <div class="row justify-content-between align-items-center">

            <!-- Header Logo (Header Left) Start -->
            <div class="header-logo col-auto">
                <a href="index.html">
                    <img src="/assets/images/logo/logo.png" alt="">
                    <img src="/assets/images/logo/logo-light.png" class="logo-light" alt="">
                </a>
            </div><!-- Header Logo (Header Left) End -->

            <!-- Header Right Start -->
            <div class="header-right flex-grow-1 col-auto">
                <div class="row justify-content-between align-items-center">

                    <!-- Side Header Toggle & Search Start -->
                    <div class="col-auto">
                        <div class="row align-items-center">

                            <!--Side Header Toggle-->
                            <div class="col-auto"><button class="side-header-toggle"><i class="zmdi zmdi-menu"></i></button></div>

                        </div>
                    </div><!-- Side Header Toggle & Search End -->

                    <!-- Header Notifications Area Start -->
                    <div class="col-auto">

                        <ul class="header-notification-area">
                            <!--Notification-->
                            <li class="adomx-dropdown col-auto">
                                <a class="toggle" href="#"><i class="zmdi zmdi-notifications"></i>
                                    @if (count(\App\Models\User::find(Auth::user()->id)->unreadNotifications) > 0)
                                        <span class="badge"></span>
                                    @endif
                                </a>

                                <!-- Dropdown -->
                                <div class="adomx-dropdown-menu dropdown-menu-notifications">
                                    <div class="head">

                                        <h5 class="title">شما {{count(\App\Models\User::find(Auth::user()->id)->unreadNotifications)}} اعلان جدید دارید.</h5>
                                    </div>
                                    <div class="body custom-scroll">
                                        <ul>
                                            @foreach(\App\Models\User::find(Auth::user()->id)->unreadNotifications as $notification)
                                                <li>
                                                    <a href="{{route('student.notify.redirect', $notification->id)}}">
                                                        <i class="zmdi zmdi-notifications-none"></i>
                                                        <div class="d-flex align-items-center justify-content-between w-100">
                                                            <p>{{$notification->data['title']}}</p>
                                                            <a class="btn btn-success px-2 py-2" href="{{route('student.notify.redirect', $notification->id)}}">مشاهده</a>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                            </li>

                            <!--User-->
                            <li class="adomx-dropdown col-auto">
                                <a class="toggle" href="#">
                                            <span class="user">
                                        <span class="avatar">
                                            <img src="/assets/images/avatar/avatar-1.jpg" alt="">
                                            <span class="status"></span>
                                            </span>
                                            <span class="name">{{auth()->user()->name}}</span>
                                            </span>
                                </a>

                                <!-- Dropdown -->
                                <div class="adomx-dropdown-menu dropdown-menu-user">
                                    <div class="head">
                                        <h5 class="name"><a href="#">{{auth()->user()->name}}</a></h5>
                                        <p class="mail">{{auth()->user()->phone}}</p>
                                    </div>
                                    <div class="body">
                                        <ul>
                                            <li><a href="{{route('student.profile')}}"><i class="zmdi zmdi-account"></i>پروفایل</a></li>
                                            <li><a href="{{route('student.profile.edit')}}"><i class="zmdi zmdi-edit"></i>ویرایش اطلاعات</a></li>
                                            <li><a href="{{route('student.profile.new.password')}}"><i class="zmdi zmdi-key"></i>تغییر رمز عبور</a></li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <form id="logout-form" action="{{route('auth.logout')}}" method="POST" style="display: none">
                                                    @csrf
                                                </form>
                                                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-lock-open"></i>خروج
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </li>

                        </ul>

                    </div><!-- Header Notifications Area End -->

                </div>
            </div><!-- Header Right End -->

        </div>
    </div>
</div><!-- Header Section End -->
<!-- Side Header Start -->
<div class="side-header show">
    <button class="side-header-close"><i class="zmdi zmdi-close"></i></button>
    <!-- Side Header Inner Start -->
    <div class="side-header-inner custom-scroll">
        <nav class="side-header-menu" id="side-header-menu">
            <ul>
                <li><a href="{{route('student.index')}}"><i class="ti-home"></i> <span>داشبورد</span></a></li>
                <li><a href="{{route('student.profile')}}"><i class="zmdi zmdi-account"></i> <span>پروفایل</span></a></li>
                <li><a href="{{route('student.bills.index')}}"><i class="zmdi zmdi-money-box"></i> <span>صورتحساب‌های من</span></a></li>
                <li><a href="{{route('student.package.store')}}"><i class="zmdi zmdi-layers"></i> <span>خرید پکیج</span></a></li>
                <li><a href="{{route('student.courses.index')}}"><i class="zmdi zmdi-collection-video"></i> <span>دوره‌های من</span></a></li>
                @canany(['academic-advice-pkg', 'debugging-pkg', 'psychology-pkg'])
                    <li><a href="{{route('student.chat.list')}}"><i class="zmdi zmdi-collection-video"></i> <span>ارتباط با مشاور / مدرس</span></a></li>
                @endcanany
                @can('exam')
                    <li><a href="{{route('student.exam.index')}}"><i class="zmdi zmdi-comments"></i> <span>آزمون‌های من</span></a></li>
                @endcan
            </ul>
        </nav>

    </div><!-- Side Header Inner End -->
</div><!-- Side Header End -->
