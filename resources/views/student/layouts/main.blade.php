<!doctype html>
<html class="no-js" lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS
	============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/vendor/bootstrap.min.css">

    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="/assets/css/vendor/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="/assets/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/vendor/themify-icons.css">
    <link rel="stylesheet" href="/assets/css/vendor/cryptocurrency-icons.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="/assets/css/plugins/plugins.css">

    <!-- Helper CSS -->
    <link rel="stylesheet" href="/assets/css/helper.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">

    <!-- Custom Style CSS Only For Demo Purpose -->
    <link id="cus-style" rel="stylesheet" href="/assets/css/style-primary.css">
    @yield('css')
</head>

<body dir="rtl">

<div class="main-wrapper">
    @include('student.layouts.nav')
    <!-- Content Body Start -->
    <div class="content-body">
        @yield('content')
    </div><!-- Content Body End -->


</div>

<!-- JS
============================================ -->
<!-- sweet alert -->
@include('sweetalert::alert')
<!-- sweet alert -->

<!-- Global Vendor, plugins & Activation JS -->
<script src="/assets/js/vendor/modernizr-3.6.0.min.js"></script>
<script src="/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="/assets/js/vendor/popper.min.js"></script>
<script src="/assets/js/vendor/bootstrap.min.js"></script>
<!--Plugins JS-->
<script src="/assets/js/plugins/perfect-scrollbar.min.js"></script>
<!--Main JS-->
<script src="/assets/js/main.js"></script>

<!-- Plugins & Activation JS For Only This Page -->

<!--Moment-->
<script src="/assets/js/plugins/moment/moment.min.js"></script>

<!--Daterange Picker-->
<script src="/assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/assets/js/plugins/daterangepicker/daterangepicker.active.js"></script>

<!--Echarts-->
<script src="/assets/js/plugins/chartjs/Chart.min.js"></script>
<script src="/assets/js/plugins/chartjs/chartjs.active.js"></script>

<!--VMap-->
<script src="assets/js/plugins/vmap/jquery.vmap.min.js"></script>
<script src="assets/js/plugins/vmap/maps/jquery.vmap.world.js"></script>
<script src="assets/js/plugins/vmap/maps/samples/jquery.vmap.sampledata.js"></script>
<script src="assets/js/plugins/vmap/vmap.active.js"></script>
@include('sweetalert::alert')
@yield('js')

</body>


</html>
