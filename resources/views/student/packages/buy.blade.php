@extends('student.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')

<div class="row">
    <!-- Advisor information -->
    <div class="col-xlg-12 col-lg-12 col-12 mb-30">
        <div class="box">
            <div class="box-head">
                <h4 class="title">خرید پکیج</h4>
            </div>
            <div class="box-body">
                <div class="alert alert-primary" role="alert">
                    <p style="text-decoration: underline;">توجه:‌ لطفا قبل از تهیه پکیج نکات زیر را با دقت مطالعه فرمایید. </p>
                    <h5>لورم ایپسوم؟</h5>
                    <p>لورم ایپسوم</p>
                </div>

                <h4 class="mb-15">پکیج موردنظر را انتخاب کنید</h4>
                <style>
                    .rb {
                        color: #475569;
                        background-color: #F1F5F9 !important;
                        border: 1px solid rgba(203,213,225);
                        border-radius: 0.5rem;
                        padding: 2rem;
                        white-space: nowrap;
                        text-align: center;
                    }
                    .rb:hover {
                        transition: 1s;
                        box-shadow: 1px 16px 19px 0px rgba(0,0,0,0.07);
                        -webkit-box-shadow: 1px 16px 19px 0px rgba(0,0,0,0.07);
                        -moz-box-shadow: 1px 16px 19px 0px rgba(0,0,0,0.07);
                    }

                </style>
                <div class="row">
                    <div class="col-10 d-flex justify-content-between">
                        @foreach ($packages as $package)
                            <div onClick="showModal('{{$package->id}}')" class="rb mr-2" style="cursor: pointer;">
                                <h3>{{$package->price}} تومان</h3>
                                <h6>پکیج {{$package->title}}</h6>
                                <p>یک ماهه</p>
                            </div>

                            <div class="modal fade" id="myModal{{$package->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header d-flex">
                                            <h5 class="modal-title" id="exampleModalLongTitle">{{$package->title}}</h5>
                                            <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" id="permission{{$package->id}}" data-permission="{{$package->permissions}}">
                                            <div class="row mt-4 mr-1">
                                                <div class="col-12">
                                                    <form action="{{route('student.package.store.buy', ['package' => $package->id])}}" method="POST">
                                                        @csrf

                                                        <div id="academic-advice-pkg{{$package->id}}">
                                                            <label class="form-label">مشاور موردنظر را انتخاب کنید.</label>
                                                            <select class="form-control" name="advisor">
                                                                <option selected disabled>انتخاب کنید</option>
                                                                @foreach (\App\Models\Advisor::getAdvisorsForReservation() as $item)
                                                                    <option value="{{$item->user_id}}">
                                                                        {{$item->user->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div id="psychology-pkg{{$package->id}}">
                                                            <label class="form-label">روانشناس موردنظر را انتخاب کنید.</label>
                                                            <select class="form-control" name="psychologist">
                                                                <option selected disabled>انتخاب کنید</option>
                                                                @foreach (\App\Models\Advisor::getAdvisorsForReservation() as $item)
                                                                    <option value="{{$item->user_id}}">
                                                                        {{$item->user->name}}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <div id="debugging-pkg{{$package->id}}">

                                                            <label class="form-label">درس موردنظر را انتخاب کنید.</label>
                                                            <select class="form-control" name="lesson" id="lesson">
                                                                @foreach (\App\Models\Lesson::all() as $item)
                                                                    <option value="{{$item->id}}">
                                                                        {{$item->name_fa}}
                                                                    </option>
                                                                @endforeach
                                                            </select>

                                                            <label class="form-label">مدرس موردنظر را انتخاب کنید.</label>
                                                            <select class="form-control" name="teacher" id="teachers">

                                                            </select>



                                                        </div>

                                                        <button class="button button-lg button-success mt-3 w-100"><span>پرداخت</span></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Advisor information [END] -->

</div>

@endsection
@section('js')
<script>
    function showModal(id) {
        $("#myModal" + id).modal('show');
        var id = id;
        var permissions;
        var permArray = [];

        permissions = $("#permission" + id).data("permission");
        for (let i = 0; i < permissions.length; i++) {
            permArray.push(permissions[i].name);
        }

        if (permArray.indexOf("academic-advice-pkg") == -1) {
            $("#academic-advice-pkg" + id).hide();
        }
        if (permArray.indexOf("psychology-pkg") == -1) {
            $("#psychology-pkg" + id).hide();
        }
        if (permArray.indexOf("debugging-pkg") == -1) {
            $("#debugging-pkg" + id).hide();
        }
    }

    let id;
    const api = "/api/get-teachers/";
    $("#lesson").on('change', function () {
        id = $(this).find(':selected').val();

        $.ajax({
            type: "GET",
            url: api + id,
            dataType:"json",
            success: function(response){
                $.each(response, function(i, item) {
                    $('#teachers').append(`<option value="${item.id}"> ${item.name} </option>`);
                });
            }
        });


    });




</script>
@endsection
