@extends('student.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">صورتحساب‌های من</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex">
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>نوع محصول</th>
                                <th>نام محصول</th>
                                <th>قیمت</th>
                                <th>تاریخ خرید</th>
                                <th>تاریخ انقضاء</th>
                                <th>تاریخ ایجاد صورتحساب</th>
                                <th>وضعیت</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($orders as $order)
                            <tr>
                                <td>
                                    @switch($order->type)
                                        @case('course')
                                        دوره آموزشی
                                        @break
                                        @case('package')
                                        پکیج
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    @switch($order->type)
                                        @case('course')
                                            @foreach($order->courses as $c)
                                                {{$c->title}}
                                            @endforeach
                                        @break
                                        @case('package')
                                            @foreach($order->packages as $p)
                                                {{$p->title}}
                                            @endforeach
                                        @break
                                    @endswitch
                                </td>
                                <td> {{$order->price}} تومان</td>
                                <td>
                                    @if($order->status == 'paid')
                                        @foreach($order->payments as $p)
                                            @if ($p->status == 1)
                                                {{jdate($p->created_at)->format('%d %B %Y')}}
                                            @endif
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($order->status == 'paid')
                                        @foreach($order->payments as $p)
                                            @if ($p->status == 1)
                                                {{jdate($p->created_at)->addMonths(1)->format('%d %B %Y')}}
                                            @endif
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{jdate($order->created_at)->format('%d %B %Y')}}</td>
                                <td>
                                    @switch($order->status)
                                        @case('paid')
                                        <div class="badge badge-success">پرداخت شده</div>
                                        @break
                                        @case('unpaid')
                                        <div class="badge badge-danger">پرداخت نشده</div>
                                        @break
                                        @case('canceled')
                                        <div class="badge badge-warning">لغو شده</div>
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    @if($order->status == 'unpaid')
                                        <form action="{{route('student.bills.payment', $order->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-danger">پرداخت</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
