@extends('student.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">پیام‌های من</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex">
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>وضعیت</th>
                                <th>اقدامات</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($tickets as $ticket)
                                <tr>
                                    <td>{{$ticket->title}}</td>
                                    <td>
                                        @switch($ticket->status)
                                            @case(1)
                                                <span class="badge badge-success">باز</span>
                                                @break
                                            @case(0)
                                                <span class="badge badge-danger">بسته شده</span>
                                                @break
                                        @endswitch
                                    </td>
                                    <td>
                                        <a href="{{route('student.chat.index', $ticket->ticket_id)}}" class="btn btn-sm btn-primary">
                                            مشاهده
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
