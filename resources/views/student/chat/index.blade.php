@extends('student.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row">
        <!-- Chat App Start -->
        <div class="col-12">
            <div class="chat-app-wrap">
                <!--Chat Active Contact Start-->
                <div class="chat-active-contact">
                    <div class="chat-contact">
                        <div class="info">
                            <h5>{{$ticket->title}}</h5>
                        </div>
                    </div>
                </div>
                <!--Chat Active Contact End-->

                <!-- Chat Start -->
                <div class="chat-wrap custom-scroll mr-0 ps ps--active-y">
                    <ul class="chat-list">
                        @foreach($tickets as $ticket)
                            <li class="{{ $ticket->user->id == Auth::user()->id ? '' : 'audience'}}">
                                <div class="chat">
                                    <div class="head">
                                        <h5>{{$ticket->user->name}}
                                            <span>
                                                {{$ticket->user->id == Auth::user()->id ? '(من)' : ''}}
                                            </span>
                                        </h5>
                                        <span>{{jdate($ticket->created_at)->ago()}}</span>
                                    </div>
                                    <div class="body">
                                        <div class="content">
                                            <p>{{$ticket->body}}</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <div class="ps__rail-x" style="left: 0px; bottom: 3px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 650px; left: 1102px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 550px;"></div></div></div>
                @if ($ticket->status == 1)

                <div class="chat-submission">
                    <form action="{{route('student.chat.sendMessage', $ticket_id)}}" method="POST">
                        @csrf
                        <input type="text" name="body" placeholder="چیزی را تایپ کنید">
                        <input type="hidden" name="ticket_id" value="{{$ticket_id}}">
                        <div class="buttons">
                            <label class="file-upload button button-box button-round button-primary" for="chat-file-upload">
                                <input type="file" id="chat-file-upload" multiple="">
                                <i class="zmdi zmdi-attachment-alt"></i>
                            </label>
                            <button class="submit button button-box button-round button-primary"><i class="zmdi zmdi-mail-send"></i></button>
                        </div>
                    </form>
                </div><!-- Chat End -->
                @endif
            </div>
        </div><!-- Chat End Start -->

        <div class="mt-3 mr-3">
            <a href="https://api.whatsapp.com/send?phone={{$ticket->receiver->phone}}" class="btn btn-sm btn-success">
                <i class="fa fa-whatsapp"></i>
                ارسال پیام در WhatsApp
            </a>
        </div>

    </div>
@endsection
