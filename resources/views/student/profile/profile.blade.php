@extends('student.layouts.main')

@section('title', 'پروفایل')


@section('content')
    <div class="row">


        <div class="col-auto col-md-3 col-sm-12">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">پروفایل شخصی</h3>
                </div>
                <div class="box-body">
                    <div class="row d-flex justify-content-center align-items-center" style="flex-direction: column;">
                        <div class="avatar avatar-xxl mr-10 mb-10">
                            <img src="{{$user->student != null ? asset($user->student->file) : ''}}" alt="">
                        </div>
                        <br>
                        <h4 class="font-weight-bolder">
                            <b>{{$user->name}}</b>
                        </h4>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-start">
                        <nav class="side-header-menu" id="side-header-menu">
                            <ul>
                                <li class="{{ isActive('student.profile'), 'active'}}">
                                    <a href="{{route('student.profile')}}">
                                        <i class="zmdi zmdi-account" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">اطلاعات کاربری</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.edit'), 'active'}}">
                                    <a href="{{route('student.profile.edit')}}">
                                        <i class="zmdi zmdi-edit" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">ویرایش</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.new.password'), 'active'}}">
                                    <a href="{{route('student.profile.new.password')}}">
                                        <i class="zmdi zmdi-key" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">تغییر رمز عبور</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-auto col-md-9 col-sm-12">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">مشخصات کاربری</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>نام و نام خانوادگی:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->name}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>تلفن تماس:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->phone}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>تلفن تماس والدین:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->student != null ? $user->student->parent_phone : ''}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>پایه تحصیلی:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->student != null ? $user->student->grade : ''}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>رشته تحصیلی:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->student != null ? $user->student->field : ''}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>شهر :</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->student != null ? $user->student->city : ''}}</p>
                        </div>
                            <div class="col-md-3">
                                <h5>ایمیل :</h5>
                            </div>
                            <div class="col-md-9">
                                <p> @if(!empty($user->student->email)) {{$user->student->email}} @else ایمیل ثبت نشده @endif</p>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

