@extends('student.layouts.main')

@section('title', 'ویرایش مشخصات کاربری')


@section('content')
    <div class="row">


        <div class="col-auto col-md-3 col-sm-12">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">پروفایل شخصی</h3>
                </div>
                <div class="box-body">
                    <div class="row d-flex justify-content-center align-items-center" style="flex-direction: column;">
                        <div class="avatar avatar-xxl mr-10 mb-10">
                            <img src="{{$user->student != null ? asset('/storage/students/' . $user->student->file) : ''}}" alt="">
                        </div>
                        <br>
                        <h4 class="font-weight-bolder">
                            <b>{{$user->name}}</b>
                        </h4>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-start">
                        <nav class="side-header-menu" id="side-header-menu">
                            <ul>
                                <li class="{{ isActive('student.profile'), 'active'}}">
                                    <a href="{{route('student.profile')}}">
                                        <i class="zmdi zmdi-account" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">اطلاعات کاربری</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.edit'), 'active'}}">
                                    <a href="{{route('student.profile.edit')}}">
                                        <i class="zmdi zmdi-edit" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">ویرایش</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.new.password'), 'active'}}">
                                    <a href="{{route('student.profile.new.password')}}">
                                        <i class="zmdi zmdi-key" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">تغییر رمز عبور</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-auto col-md-9 col-sm-12">
            <div class="box">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('student.profile.update', $user)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                  <div class="row">

                    <!--Form controls Start-->
                    <div class="col-12 mb-30">
                        <div class="box">
                            <div class="box-head">
                                <h3 class="title">ویرایش مشخصات کاربری</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-4 col-12 mb-20">
                                        <h6 class="mb-15">عکس جدید</h6>
                                        <div class="row mbn-15">
                                          <input class="dropify" type="file" name="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mbn-20 mt-1">

                                    <!--Form Field-->
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">نام و نام خانوادگی</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->name}}" name="name"></div>
                                        </div>

                                    </div>
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">تلفن تماس</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->phone}}" name="phone"></div>
                                        </div>

                                    </div>
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">تلفن تماس والدین</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->student != null ? $user->student->parent_phone : ''}}" name="parent_phone"></div>
                                        </div>

                                    </div>
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">پایه تحصیلی</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->student != null ? $user->student->grade : ''}}" name="grade"></div>
                                        </div>

                                    </div>
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">رشته تحصیلی</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->student != null ? $user->student->field : ''}}" name="field"></div>
                                        </div>

                                    </div>
                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">شهر</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="text" class="form-control"
                                                                             value="{{$user->student != null ? $user->student->city : ''}}" name="city"></div>
                                        </div>

                                    </div>

                                    <div class="col-lg-4 col-12 mb-20">

                                        <h6 class="mb-15">ایمیل</h6>

                                        <div class="row mbn-15">
                                            <div class="col-12 mb-15"><input type="email" class="form-control"
                                                                             value="{{$user->student != null ? $user->student->email : ''}}" name="email"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row mbn-20 m-5">
                                    <button class="button button-success"><span>ذخیره</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Form controls End-->
                </div>
                </form>
            </div>

        </div>

    </div>
@endsection
@section('js')
    <script src="/assets/js/plugins/filepond/filepond.min.js"></script>
    <script src="/assets/js/plugins/filepond/filepond-plugin-image-exif-orientation.min.js"></script>
    <script src="/assets/js/plugins/filepond/filepond-plugin-image-preview.min.js"></script>
    <script src="/assets/js/plugins/filepond/filepond.active.js"></script>
    <script src="/assets/js/plugins/dropify/dropify.min.js"></script>
    <script src="/assets/js/plugins/dropify/dropify.active.js"></script>
    <script>
        ('.dropify').dropify();
    </script>
@endsection
