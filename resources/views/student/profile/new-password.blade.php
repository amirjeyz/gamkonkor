@extends('student.layouts.main')

@section('title', 'تغییر رمز عبور')


@section('content')
    <div class="row">


        <div class="col-auto col-md-3 col-sm-12">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">پروفایل شخصی</h3>
                </div>
                <div class="box-body">
                    <div class="row d-flex justify-content-center align-items-center" style="flex-direction: column;">
                        <div class="avatar avatar-xxl mr-10 mb-10">
                            <img src="" alt="">
                        </div>
                        <br>
                        <h4 class="font-weight-bolder">
                            <b>{{$user->name}}</b>
                        </h4>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-start">
                        <nav class="side-header-menu" id="side-header-menu">
                            <ul>
                                <li class="{{ isActive('student.profile'), 'active'}}">
                                    <a href="{{route('student.profile')}}">
                                        <i class="zmdi zmdi-account" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">اطلاعات کاربری</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.edit'), 'active'}}">
                                    <a href="{{route('student.profile.edit')}}">
                                        <i class="zmdi zmdi-edit" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">ویرایش</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('student.profile.new.password'), 'active'}}">
                                    <a href="{{route('student.profile.new.password')}}">
                                        <i class="zmdi zmdi-key" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">تغییر رمز عبور</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-auto col-md-9 col-sm-12">
            <div class="box">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{route('student.profile.update.password', $user)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="row">

                        <!--Form controls Start-->
                        <div class="col-12 mb-30">
                            <div class="box">
                                <div class="box-head">
                                    <h3 class="title">تغییر رمز عبور</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row mbn-20 mt-1">

                                        <!--Form Field-->
                                        <div class="col-lg-12 col-12 mb-20">

                                            <h6 class="mb-15">رمز جدید</h6>

                                            <div class="row mbn-15">
                                                <div class="col-12 mb-15"><input type="password" class="form-control"
                                                                                  name="password"></div>
                                            </div>

                                        </div>

                                        <div class="col-lg-12 col-12 mb-20">

                                            <h6 class="mb-15">تایید رمز جدید</h6>

                                            <div class="row mbn-15">
                                                <div class="col-12 mb-15"><input type="password" class="form-control"
                                                                                 name="password_confirmation"></div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row mbn-20 m-5">
                                        <button class="button button-success"><span>ذخیره</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Form controls End-->
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection

