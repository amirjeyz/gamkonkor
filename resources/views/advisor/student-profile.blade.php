@extends('advisor.layouts.main')

@section('title', 'اطلاعات دانش آموز')
@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">پروفایل کاربر {{$user->name}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="font-weight-bold">نام و نام خانوادگی:</td>
                                    <td>&nbsp;{{$user->name}}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">پایه تحصیلی:</td>
                                    <td>{{$user->student->grade}}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">رشته تحصیلی:</td>
                                    <td>{{$user->student->field}}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">تلفن تماس دانش آموز:</td>
                                    <td>&nbsp;{{$user->phone}}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">تلفن تماس والدین دانش آموز:</td>
                                    <td>&nbsp;{{$user->student->parent_phone}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 col-md-12 col-lg-5">
                            <table class="ml-0 ml-sm-0 ml-lg-0">
                                <tbody>
                                <tr>
                                    <td class="font-weight-bold">شهر:</td>
                                    <td>&nbsp;{{$user->student->city}}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">ایمیل:</td>
                                    <td>
                                        @if(empty($user->student->email))
                                            <span class="text-warning"> بدون ایمیل</span>
                                        @else
                                            {{$user->student->email}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">	اتمام دوره مشاوره در:</td>
                                    <td>
                                        &nbsp; {{jdate($user->student->expire)->format('Y-m-d')}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
