@extends('advisor.layouts.main')

@section('title', 'پروفایل')


@section('content')
    <div class="row">


        <div class="col-3">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">پروفایل شخصی</h3>
                </div>
                <div class="box-body">
                    <div class="row d-flex justify-content-center align-items-center" style="flex-direction: column;">
                        <div class="avatar avatar-xxl mr-10 mb-10">
                            {{--                            <img src="{{asset('/storage/students/' . $user->student->file)}}" alt="">--}}
                        </div>
                        <br>
                        <h4 class="font-weight-bolder">
                            <b>{{$user->name}}</b>
                        </h4>
                    </div>
                    <hr>
                    <div class="d-flex justify-content-start">
                        <nav class="side-header-menu" id="side-header-menu">
                            <ul>
                                <li class="{{ isActive('advisor.profile'), 'active'}}">
                                    <a href="{{route('advisor.profile')}}">
                                        <i class="zmdi zmdi-account" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">اطلاعات کاربری</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('advisor.profile.edit'), 'active'}}">
                                    <a href="{{route('advisor.profile.edit')}}">
                                        <i class="zmdi zmdi-edit" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">ویرایش</span>
                                    </a>
                                </li>
                                <li class="{{ isActive('advisor.profile.new.password'), 'active'}}">
                                    <a href="{{route('advisor.profile.new.password')}}">
                                        <i class="zmdi zmdi-key" style="font-size: 20px;"></i>
                                        <span style="font-size: 15px; font-weight: 300;">تغییر رمز عبور</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-9">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">مشخصات کاربری</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>نام و نام خانوادگی:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->name}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>تلفن تماس:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->phone}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>رتبه کنکور:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->exam_rank}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>رشته تحصیلی:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->field}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>دانشگاه:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->university}}</p>
                        </div>
                        <div class="col-md-3">
                            <h5>سابقه(سال):</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->experience_year}} سال</p>
                        </div>
                        <div class="col-md-3">
                            <h5>شهر:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->city}} </p>
                        </div>
                        <div class="col-md-3">
                            <h5>آدرس:</h5>
                        </div>
                        <div class="col-md-9">
                            <p>{{$user->advisor->address}} </p>
                        </div>
                        <div class="col-md-3">
                            <h5>ایمیل:</h5>
                        </div>
                        <div class="col-md-9">
                            <p> {{$user->advisor->email}}</p>
                        </div>
                        @if(!empty($user->advisor->experience_text))
                            <div class="col-md-3">
                                <h5>سابقه(متن):</h5>
                            </div>
                            <div class="col-md-9">
                                <p> {{$user->advisor->experience_text}}</p>
                            </div>
                        @endif
                        @if(!empty($user->advisor->experience_file))
                            <div class="col-md-3">
                                <h5>سابقه(فایل):</h5>
                            </div>
                            <div class="col-md-9">
                                <form action="{{route('admin.users.download.file')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="path"
                                           value="{{$user->advisor->experience_file}}">
                                    <button type="submit"
                                            class="btn btn-success mr-1 waves-effect waves-light">دانلود
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

