@extends('advisor.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">دستمزد‌های من</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex">
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>عنوان دستمزد</th>
                                <th>توضیحات</th>
                                <th>تاریخ شروع</th>
                                <th>تاریخ پایان</th>
                                <th>مقدار دستمزد واریزی</th>
                            </tr>
                            @foreach ($salaries as $salary)
                            <tr>
                                <td>{{$salary->title}}</td>
                                <td>{{$salary->description}}</td>
                                <td>{{jdate($salary->start_date)->format('%d %B %Y')}}</td>
                                <td>{{jdate($salary->end_date)->format('%d %B %Y')}}</td>
                                <td>{{$salary->price}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
