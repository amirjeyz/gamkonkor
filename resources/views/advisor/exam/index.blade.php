@extends('advisor.layouts.main')

@section('title', 'لیست آزمون‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست آزمون‌ها</h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex">
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>عنوان آزمون</th>
                                <th>دانش آموز</th>
                                <th>اقدامات</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach ($exams as $exam)
                                <tr>
                                    <td>{{$exam->name}}</td>
                                    <td>{{$exam->student->name}}</td>
                                    <td class="d-flex justify-content-start">
                                        @switch($exam->status)
                                            @case(0)
                                                <form action="{{route('advisor.exam.start', $exam->id)}}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-success">شروع‌ آزمون</button>
                                                </form>
                                                @break
                                            @case(1)
                                                <form action="{{route('advisor.exam.stop', $exam->id)}}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-warning">پایان آزمون</button>
                                                </form>
                                                @break
                                            @case(2)
                                                <button type="button" class="btn btn-sm btn-light" data-toggle="modal" data-target="#exampleModalCenter{{$exam->id}}">
                                                    مشاهده کارنامه دانش آموز
                                                </button>
                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter{{$exam->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">کارنامه {{$exam->name}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col">نام درس</th>
                                                                        <th scope="col">تعداد سوالات</th>
                                                                        <th scope="col">درصد</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($exam->generateResult() as $item)
                                                                        <tr>
                                                                            <td>{{$item['name']}}</td>
                                                                            <td>{{$item['total']}}</td>
                                                                            <td>{{$item['darsad']}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @break
                                        @endswitch
                                        <form action="{{route('advisor.exam.delete', $exam->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-danger mr-2">حذف آزمون</button>
                                        </form>



                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
