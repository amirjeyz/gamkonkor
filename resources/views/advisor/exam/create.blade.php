@extends('advisor.layouts.main')

@section('title', 'ایجاد آزمون‌')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12 mb-30">
            <div class="box">
                <div class="box-head">
                    <h3 class="title">متن کمکی</h3>
                </div>

                <form action="{{route('advisor.exam.store')}}" method="POST">
                    @csrf
                    <div class="box-body">
                        <div class="row mbn-15">

                            <div class="col-12 mb-15">
                                <div class="form-group">
                                    <label>:نام ازمون</label>
                                    <input name="quiz_name" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>تاریخ و ساعت انقضا آزمون:</label>
                                    <input name="expired_at" type="datetime-local" class="form-control" id="expired_time">
                                </div>
                                <div class="form-group">
                                    <label>مهلت آزمون:</label>
                                    <small class="text-danger">(لطفا به دقیقه وارد کنید.)</small>
                                    <input name="end_time" type="number" class="form-control" id="expired_time">
                                </div>

                                <div class="form-group mt-4">
                                    <label>دانش‌ آموز:</label>
                                    <select name="student_id" class="form-control">
                                        <option value="0" disabled selected>لطفا دانش آموز موردنظر خود را انتخاب کنید.</option>
                                        @foreach ($students as $student)
                                            <option value="{{$student->id}}">{{$student->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <hr>
                                <div id="add_input_to_form">

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success add_newinputs">اضافه کردن سوال</button>
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
var tedadesoal;
    tedadesoal = 0;
    $(document).on("click", ".add_newinputs", function() {
            tedadesoal++;
            $("#add_input_to_form").append(`
                    <h2 class="text-center">سوال
                          `+ tedadesoal +`
                    </h2>
                    <div class="form-group">
                        <label>سوال `+ tedadesoal +`</label>
                        <textarea class="ckeditor form-control" name="q[]"></textarea>
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>گزینه اول</label>
                        <input name="option1[]" type="text" class="form-control">
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>گزینه دوم</label>
                        <input name="option2[]" type="text" class="form-control">
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>گزینه سوم</label>
                        <input name="option3[]" type="text" class="form-control">
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>گزینه چهارم</label>
                        <input name="option4[]" type="text" class="form-control">
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>کدام پاسخ درسته؟</label>
                        <select name="is_correct[]" class="form-control">
                          <option value="0" disabled selected>گزینه موردنظر را وارد کنید.</option>
                          <option value="1">گزینه ۱</option>
                          <option value="2">گزینه ۲</option>
                          <option value="3">گزینه ۳</option>
                          <option value="4">گزینه ۴</option>
                        </select>
                    </div>
                    <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                        <label>سوال مربوط به کدام درس است؟</label>
                        <select name="type[]" class="form-control">
                          <option value="0" disabled selected>گزینه موردنظر را وارد کنید.</option>
                            @foreach ($types as $type)
                                <option value="{{$type->id}}">{{$type->name_fa}}</option>
                            @endforeach
                        </select>
                    </div>
                    <hr>
        `)
        CKEDITOR.replaceAll('ckeditor', {
            filebrowserUploadUrl: "{{route('admin.exam.ckeditor', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    });
</script>
@endsection
