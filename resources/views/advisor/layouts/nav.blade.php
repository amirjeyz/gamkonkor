<!-- Header Section Start -->
<div class="header-section">
    <div class="container-fluid">
        <div class="row justify-content-between align-items-center">

            <!-- Header Logo (Header Left) Start -->
            <div class="header-logo col-auto">
                <a href="#">
                    <img src="/assets/images/logo/logo.png" alt="">
                    <img src="/assets/images/logo/logo-light.png" class="logo-light" alt="">
                </a>
            </div><!-- Header Logo (Header Left) End -->

            <!-- Header Right Start -->
            <div class="header-right flex-grow-1 col-auto">
                <div class="row justify-content-between align-items-center">

                    <!-- Side Header Toggle & Search Start -->
                    <div class="col-auto">
                        <div class="row align-items-center">

                            <!--Side Header Toggle-->
                            <div class="col-auto"><button class="side-header-toggle"><i class="zmdi zmdi-menu"></i></button></div>

                        </div>
                    </div><!-- Side Header Toggle & Search End -->

                    <!-- Header Notifications Area Start -->
                    <div class="col-auto">

                        <ul class="header-notification-area">
                            <!--Notification-->
                            <li class="adomx-dropdown col-auto">
                                <a class="toggle" href="#"><i class="zmdi zmdi-notifications"></i>
                                    @if (count(\App\Models\User::find(\Illuminate\Support\Facades\Auth::user()->id)->unreadNotifications) > 0)
                                        <span class="badge"></span>
                                    @endif
                                </a>

                                <!-- Dropdown -->
                                <div class="adomx-dropdown-menu dropdown-menu-notifications">
                                    <div class="head">
                                        <h5 class="title">شما {{count(\App\Models\User::find(\Illuminate\Support\Facades\Auth::user()->id)->unreadNotifications)}} اعلان جدید دارید.</h5>
                                    </div>
                                    <div class="body custom-scroll">
                                        <ul>
                                            @foreach(\App\Models\User::find(Auth::user()->id)->unreadNotifications as $notification)
                                                <li>
                                                    <a href="{{route('student.notify.redirect', $notification->id)}}">
                                                        <i class="zmdi zmdi-notifications-none"></i>
                                                        <div class="d-flex align-items-center justify-content-between w-100">
                                                            <p>{{$notification->data['title']}}</p>
                                                            <a class="btn btn-success px-2 py-2" href="{{route('student.notify.redirect', $notification->id)}}">مشاهده</a>
                                                        </div>
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                            </li>

                            <!--User-->
                            <li class="adomx-dropdown col-auto">
                                <a class="toggle" href="#">
                                            <span class="user">
                                        <span class="avatar">
                                            <img src="/assets/images/avatar/avatar-1.jpg" alt="">
                                            <span class="status"></span>
                                            </span>
                                            <span class="name">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                            </span>
                                </a>

                                <!-- Dropdown -->
                                <div class="adomx-dropdown-menu dropdown-menu-user">
                                    <div class="head">
                                        <h5 class="name"><a href="#">{{\Illuminate\Support\Facades\Auth::user()->name}}</a></h5>
                                        <a class="mail" href="#">{{\Illuminate\Support\Facades\Auth::user()->phone}}</a>
                                    </div>
                                    <div class="body">
                                        <ul>
                                            <li><a href="#"><i class="zmdi zmdi-account"></i>پروفایل</a></li>
                                        <ul>
                                            <li>
                                                <form id="logout-form" action="{{route('auth.logout')}}" method="POST" style="display: none">
                                                    @csrf
                                                </form>
                                                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-lock-open"></i>خروج
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </li>

                        </ul>

                    </div><!-- Header Notifications Area End -->

                </div>
            </div><!-- Header Right End -->

        </div>
    </div>
</div><!-- Header Section End -->
<!-- Side Header Start -->
<div class="side-header show">
    <button class="side-header-close"><i class="zmdi zmdi-close"></i></button>
    <!-- Side Header Inner Start -->
    <div class="side-header-inner custom-scroll">
        <nav class="side-header-menu" id="side-header-menu">
            <ul>
                <li><a href="{{route('advisor.index')}}"><i class="ti-home"></i> <span>داشبورد</span></a></li>
                <li><a href="{{route('advisor.profile')}}"><i class="zmdi zmdi-account"></i> <span>پروفایل</span></a></li>
                <li><a href="{{route('salaries.index')}}"><i class="zmdi zmdi-money-box"></i> <span>پرداختی‌های من</span></a></li>
                <li class="has-sub-menu">
                    <a href="#">
                        <i class="ti-book"></i> <span>آزمون ساز</span><span class="menu-expand">
                            <i class="zmdi zmdi-chevron-up"></i>
                        </span>
                    </a>
                    <ul class="side-header-sub-menu" style="display: block;">
                        <li><a href="{{route('advisor.exam.index')}}"><span>لیست آزمون‌ها</span></a></li>
                        <li><a href="index-crypto.html"><span>ایجاد آزمون</span></a></li>
                    </ul>
                </li>
            </ul>
        </nav>

    </div><!-- Side Header Inner End -->
</div><!-- Side Header End -->
