@extends('advisor.layouts.main')

@section('title', 'داشبورد')

@section('content')
    <div class="row">
        <div class="col-lg-12 col-12 mb-30">
            <div class="box">
                <div class="box-head">
                    <h4 class="title">لیست دانش آموزان</h4>
                </div>
                <div class="box-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>نام و نام خانوادگی</th>
                            <th>شماره تماس</th>
                            <th>اتمام دوره مشاوره در</th>
                            <th>اقدامات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->students as $student)
                        <tr>
                            <td>{{$student->user->name}}</td>
                            <td>{{$student->user->phone}}</td>
                            <td>{{jdate($student->expire)->format('Y-m-d')}}</td>
                            <td>
                                <a href="{{route('advisor.studentProfile', $student->user)}}" class="button button-primary">
                                    مشاهده
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

