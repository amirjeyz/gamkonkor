<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                ایجاد ازمون
            </div>
            <div class="card-body">
                <form action="" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>نام ازمون</label>
                        <input name="quiz_name" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <input name="expired_at" type="datetime-local" class="form-control">
                    </div>
                    <hr>




                    <div id="add_input_to_form">
{{--                        <h2 class="text-center">سوال 1</h2>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>سوال اول</label>--}}
{{--                            <input name="q[]" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>پاسخ اول</label>--}}
{{--                            <input name="option1" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>پاسخ اول</label>--}}
{{--                            <input name="option2" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>پاسخ اول</label>--}}
{{--                            <input name="option3" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>پاسخ اول</label>--}}
{{--                            <input name="option4" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label>کدام پاسخ درسته؟</label>--}}
{{--                            <input name="is_correct" type="text" class="form-control">--}}
{{--                        </div>--}}
{{--                        <hr>--}}
                    </div>



                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-success add_newinputs">add</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        var tedadesoal;
        tedadesoal = 0;
        $(".add_newinputs").on("click", function() {
                tedadesoal++;
                $("#add_input_to_form").append(`
                        <h2 class="text-center">سوال
                              `+ tedadesoal +`
                        </h2>
                        <div class="form-group">
                            <label>سوال `+ tedadesoal +`</label>
                            <input name="q[]" type="text" class="form-control">
                        </div>
                        <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                            <label>گزینه اول</label>
                            <input name="option1" type="text" class="form-control">
                        </div>
                        <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                            <label>گزینه دوم</label>
                            <input name="option1" type="text" class="form-control">
                        </div>
                        <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                            <label>گزینه سوم</label>
                            <input name="option1" type="text" class="form-control">
                        </div>
                        <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                            <label>گزینه چهارم</label>
                            <input name="option1" type="text" class="form-control">
                        </div>
                        <div class="form-group" style="width:80%; margin-left: auto; margin-right: auto;">
                            <label>کدام پاسخ درسته؟</label>
                            <select name="is_correct" class="form-control">
                              <option value="0" disabled selected>گزینه موردنظر را وارد کنید.</option>
                              <option value="1">گزینه ۱</option>
                              <option value="2">گزینه ۲</option>
                              <option value="3">گزینه ۳</option>
                              <option value="4">گزینه ۴</option>
                            </select>
                        </div>
                        <hr>
            `)
        });

    </script>
</body>
</html>
