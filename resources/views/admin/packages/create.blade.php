@extends('admin.layouts.main')

@section('title', 'ایجاد دوره')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد دوره جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- form start -->
                <form action="{{route('admin.packages.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>عنوان پکیج</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>توضیحات پکیج</label>
                            <textarea type="text" name="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>قیمت</label>
                            <input type="text" name="price" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>مدت زمان انقضا پکیج</label>
                            <select class="form-control" name="expire_time">
                                <option value="0" selected disabled>لطفا مدت زمان را وارد کنید.</option>
                                <option value="1">یک ماه</option>
                                <option value="2">دو ماه</option>
                                <option value="3">سه ماه</option>
                                <option value="6">شش ماه</option>
                                <option value="12">یک سال</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>دسترسی‌ها</label>
                            <br>
                            <small class="text-danger">لطفا دسترسی‌های موردنظر برای این پکیج را وارد کنید.</small>
                            <select multiple="" class="form-control" name="permissions[]">
                                @foreach(\App\Models\Permission::all() as $permission)
                                    <option value="{{$permission->id}}">{{$permission->getPersianName()}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
