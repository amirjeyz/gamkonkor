
@extends('admin.layouts.main')

@section('title', 'لیست پکیح‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست پکیج‌ها</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>کد یکتا پکیح</th>
                                <th>نام پکیح</th>
                                <th>قیمت</th>
                                <th>تاریخ ثبت</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($packages as $package)
                                <tr>
                                    <td>{{$package->id}}</td>
                                    <td>{{$package->title}}</td>
                                    <td>{{$package->price}} ریال</td>
                                    <td>{{jdate($package->created_at)->format('Y-m-d')}}</td>
                                    <td class="d-flex justify-content-start">
                                        <a class="btn btn-primary btn-sm" href="{{route('admin.packages.edit', $package->id)}}">ویرایش پکیح</a>

                                        <form action="{{route('admin.packages.delete', $package->id)}}" method="POST">
                                            @csrf
                                            <button class="btn btn-danger btn-sm mr-1">حذف پکیح</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
