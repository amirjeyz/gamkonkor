@extends('admin.layouts.main')

@section('title', 'ایجاد دوره')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ویرایش پکیج {{$package->title}}</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form action="{{route('admin.packages.update', $package->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label>عنوان پکیج</label>
                            <input type="text" name="title" class="form-control" value="{{old('title', $package->title)}}">
                        </div>
                        <div class="form-group">
                            <label>توضیحات پکیج</label>
                            <textarea type="text" name="description" class="form-control">{{$package->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>قیمت</label>
                            <input type="text" name="price" class="form-control" value="{{old('price', $package->price)}}">
                        </div>

                        <div class="form-group">
                            <label>آپلود کاور</label>
                            <input type="file" name="cover" class="form-control">
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
