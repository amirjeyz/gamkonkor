@extends('admin.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست دوره‌ها</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>کد یکتا دوره</th>
                                <th>نام دوره</th>
                                <th>تاریخ ثبت</th>
                                <th>وضعیت دوره</th>
                                <th>نام مدرس</th>
                                <th>تعداد شرکت کنندگان</th>
                                <th>تعداد قسمت‌ها</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($courses as $course)
                                <tr>
                                    <td>{{$course->id}}</td>
                                    <td>{{$course->title}}</td>
                                    <td>{{jdate($course->created_at)->format('Y-m-d')}}</td>
                                    <td>{{$course->status}}</td>
                                    <td>{{$course->teacher_name}}</td>
                                    <td>{{count($course->orders)}}</td>
                                    <td>{{count($course->episodeHa)}}</td>
                                    <td>
                                        <form action="{{route('admin.courses.destroy', $course->id)}}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger">حذف دوره</button>
                                        </form>
                                        <a class="btn btn-primary" href="{{route('admin.courses.edit', $course->id)}}">ویرایش دوره</a>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addVideoModal-{{$course->id}}">
                                            ویدیو‌ها
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="addVideoModal-{{$course->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header d-flex">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">ویدیو‌ها</h5>
                                                        <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <ul class="list-group list-group-flush">
                                                            @foreach($course->episodeHa as $episode)

                                                            <li class="list-group-item">
                                                                {{$episode->title}}
                                                                <a href="" class="btn btn-sm btn-info">مشاهده</a>
                                                                <a href="{{route('admin.episode.edit', $episode->id)}}" class="btn btn-sm btn-primary">ویرایش</a>
                                                                <form method="POST" action="{{route('admin.episode.delete', $episode->id)}}">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-sm btn-danger">حذف</button>
                                                                </form>
                                                            </li>
                                                            @endforeach

                                                        </ul>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                                                        <a href="{{route('admin.episode.add', $course->id)}}" class="btn btn-success mr-1">اضافه کردن</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
