@extends('admin.layouts.main')

@section('title', 'ویرایش دوره')

@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد دوره جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form action="{{route('admin.courses.update', $course->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label>عنوان دوره</label>
                            <input type="text" name="title" class="form-control" value="{{$course->title}}">
                        </div>

                        <div class="form-group">
                            <label>توضیح کوتاه دوره</label>
                            <textarea name="description" class="form-control" >{{$course->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>متن کامل دوره</label>
                            <textarea name="content" class="form-control">{{$course->content}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>قیمت دوره</label>
                            <input type="text" name="price" class="form-control" value="{{$course->price}}">
                        </div>

                        <div class="form-group">
                            <label>تعداد قسمت‌ها</label>
                            <input type="text" name="episodes" class="form-control" value="{{$course->episodes}}">
                        </div>

                        <div class="form-group">
                            <label>زمان دوره</label>
                            <input type="text" name="course_time" class="form-control" value="{{$course->course_time}}">
                        </div>

                        <div class="form-group">
                            <label>وضعیت دوره</label>
                            <select name="status" class="form-control">
                                <option value="0" disabled selected>وضعیت دوره را انتخاب کنید</option>
                                <option value="completed">
                                    کامل شده
                                </option>
                                <option value="uncomplete">
                                    درحال ضبط
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>نام مدرس</label>
                            <input type="text" name="teacher_name" class="form-control" value="{{$course->teacher_name}}">
                        </div>

                        <div class="form-group">
                            <label>آپلود کاور</label>
                            <input type="file" name="cover" class="form-control" value="{{$course->cover}}">
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
