@extends('admin.layouts.main')

@section('title', 'ویدیو‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ویرایش {{$episode->title}}</h3>
                    <p class="text-gray">دوره:
                        {{$courseName}}
                    </p>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form action="{{route('admin.episode.update', $episode->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label>عنوان</label>
                            <input type="text" name="title" class="form-control" value={{$episode->title}}>
                        </div>

                        <div class="form-group">
                            <label>توضیحات</label>
                            <textarea name="description" class="form-control" rows="5">{{$episode->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label>نام مدرس</label>
                            <input type="text" name="teacher_name" class="form-control" value="{{$episode->teacher_name}}">
                        </div>
                        <div class="form-group">
                            <label>قیمت</label>
                            <input type="text" name="price" class="form-control" value="{{$episode->price}}">
                        </div>

                        <div class="form-group">
                            <label>آپلود ویدیو</label>
                            <input type="file" name="video_link" class="form-control">
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

