@extends('admin.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست دوره‌ها</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>عنوان آزمون</th>
                                <th>تعداد شرکت‌کنندگان</th>
                                <th>تاریخ ثبت</th>
                                <th>تاریخ اتمام آزمون</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($exams as $exam)
                                <tr>
                                    <td>{{$exam->name}}</td>
                                    <td>
                                        {{\App\Models\Quiz::countUserAnswer($exam->id) ?? '0'}}
                                    </td>
                                    <td>{{jdate($exam->created_at)->format('Y/m/d')}}</td>
                                    <td>{{jdate($exam->expired_at)->format('Y/m/d H:i')}}</td>
                                    <td class="d-flex justify-content-around">
                                        @switch($exam->status)
                                            @case(0)
                                                <form action="{{route('admin.exam.start', $exam->id)}}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-success">شروع‌ آزمون</button>
                                                </form>
                                                @break
                                            @case(1)
                                                <form action="{{route('admin.exam.stop', $exam->id)}}" method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-sm btn-warning">پایان آزمون</button>
                                                </form>
                                                @break
                                            @case(2)
                                                @break
                                        @endswitch

                                        <form action="{{route('admin.exam.delete', $exam->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-sm btn-danger">حذف</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
