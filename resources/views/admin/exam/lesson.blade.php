@extends('admin.layouts.main')

@section('title', 'لیست دروس')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست دورس</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-sm btn-success" style="border-radius: 5px !important;" data-toggle="modal" data-target="#create">
                                    ایجاد درس جدید
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header d-flex">
                                                <h5 class="modal-title" id="exampleModalLongTitle">ایجاد درس جدید</h5>
                                                <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="{{route('admin.exam.lesson.store')}}" method="POST">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label>نام درس: </label>
                                                        <input type="text" name="name_fa" class="form-control" >
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-sm btn-success ml-1">ثبت</button>
                                                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>عنوان درس</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach ($lessons as $lesson)
                                <tr>
                                    <td>{{$lesson->name_fa}}</td>
                                    <td>
                                        <form action="">
                                            <button type="submit" class="btn btn-sm btn-danger">حذف</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
