<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" style="direction: ltr">
        <div style="direction: rtl">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="info">
                    <span class="d-block text-white">{{auth()->user()->name}}</span>
                    <form method="POST" action="{{route('auth.logout')}}">
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm text-white mt-2 w-100">خروج</button>
                    </form>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-item">
                        <a href="{{route('admin.home')}}" class="nav-link {{ isActive('admin.home'), 'active'}}">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                داشبورد
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview {{ isOpen(['admin.users.list', 'admin.users.create']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-users"></i>
                            <p>
                                مدیریت کاربران
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.users.list')}}" class="nav-link {{ isActive('admin.users.list'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست کاربران</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.users.create')}}" class="nav-link {{ isActive('admin.users.create'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد کاربر</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview {{ isOpen(['admin.courses.index', 'admin.courses.create']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-shopping-cart"></i>
                            <p>
                                مدیریت فروشگاه
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.courses.index')}}" class="nav-link {{ isActive('admin.courses.index'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست دوره‌ها</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.courses.create')}}" class="nav-link {{ isActive('admin.courses.create'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد دوره</p>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview {{ isOpen(['admin.packages.index', 'admin.packages.create']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-shopping-basket"></i>
                            <p>
                                مدیریت پکیج‌ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.packages.index')}}" class="nav-link {{ isActive('admin.packages.index'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست پکیج‌ها</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.packages.create')}}" class="nav-link {{ isActive('admin.packages.create'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد پکیج</p>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview {{ isOpen(['admin.vote.index', 'admin.vote.create']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-bar-chart-o"></i>
                            <p>
                                نظرسنجی
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.vote.index')}}" class="nav-link {{ isActive('admin.vote.index'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست نظرسنجی‌ها</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.vote.create')}}" class="nav-link {{ isActive('admin.vote.create'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد نظرسنجی</p>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview {{ isOpen(['admin.bills.index', 'admin.bills.createSalary']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-calculator"></i>
                            <p>
                                حسابداری
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.bills.index')}}" class="nav-link {{ isActive('admin.bills.index'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست صورتحساب‌ها</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.bills.createSalary')}}" class="nav-link {{ isActive('admin.bills.createSalary'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد واریزی مشاور</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                <li class="nav-item has-treeview {{ isOpen(['admin.exam.index', 'admin.exam.create', 'admin.exam.lesson']), 'menu-open' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-question"></i>
                            <p>
                                آزمون‌ساز
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('admin.exam.index')}}" class="nav-link {{ isActive('admin.exam.index'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست آزمون‌ها</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.exam.create')}}" class="nav-link {{ isActive('admin.exam.create'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ایجاد آزمون</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.exam.lesson')}}" class="nav-link {{ isActive('admin.exam.lesson'), 'active'}}">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت دروس</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.settings')}}" class="nav-link {{ isActive('admin.settings'), 'active'}}">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                تنظیمات وبسایت
                            </p>
                        </a>
                    </li>


                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
