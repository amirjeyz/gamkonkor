@extends('admin.layouts.main')

@section('title', 'لیست نظرسنجی‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست پاسخ‌های نظرسنجی </h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>نام دانش آموز</th>
                            <th>نام مشاور</th>
                            <th>میانگین نمره</th>
                            <th>پاسخ‌ها</th>
                        </tr>
                        @foreach ($votes as $item)
                            @foreach ($item->answers as $answer)
                            <tr>
                                <td>{{$answer->user->name}}</td>
                                <td>{{$answer->adviser->name}}</td>
                                <td>{{$answer->sum}} از 10</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#showAnswer-{{$answer->id}}">
                                        مشاهده
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="showAnswer-{{$answer->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header d-flex">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">لیست پاسخ‌های {{$answer->user->name}}</h5>
                                                    <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    @foreach ($votes as $vote)
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question1}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer1}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question2}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer2}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question3}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer3}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question4}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer4}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question5}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer5}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question6}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer6}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question7}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer7}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question8}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer8}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question9}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer9}}</div>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between w-100">
                                                            <p>{{$vote->question10}}</p>
                                                            <div class="btn btn-primary btn-sm h-50" style="cursor: default;">{{$answer->answer10}}</div>
                                                        </li>
                                                    @endforeach
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
