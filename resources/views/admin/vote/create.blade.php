@extends('admin.layouts.main')

@section('title', 'ایجاد نظرسنجی')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد نظرسنجی جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- form start -->
                <form action="{{route('admin.vote.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>عنوان نظرسنجی</label>
                            <input type="text" name="title" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>سوال یک</label>
                            <input type="text" name="question1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال دو</label>
                            <input type="text" name="question2" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال سه</label>
                            <input type="text" name="question3" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال چهار</label>
                            <input type="text" name="question4" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال پنج</label>
                            <input type="text" name="question5" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال شش</label>
                            <input type="text" name="question6" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال هفت</label>
                            <input type="text" name="question7" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال هشت</label>
                            <input type="text" name="question8" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال نه</label>
                            <input type="text" name="question9" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>سوال ده</label>
                            <input type="text" name="question10" class="form-control">
                        </div>

                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ایجاد</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
