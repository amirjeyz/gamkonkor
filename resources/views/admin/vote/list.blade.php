@extends('admin.layouts.main')

@section('title', 'لیست نظرسنجی‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست نظرسنجی‌ها</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>آیدی</th>
                                <th>عنوان</th>
                                <th>سوالات</th>
                                <th>نظرسنجی برای</th>
                                <th>تاریخ ارسال نظرسنجی</th>
                                <th>پاسخ دهنده‌ها</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach ($votes as $vote)
                                <tr>
                                    <td>{{$vote->id}}</td>
                                    <td>{{$vote->title}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#showAnswer-{{$vote->id}}">
                                            مشاهده
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="showAnswer-{{$vote->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header d-flex">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">لیست سوالات</h5>
                                                        <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                        <div class="modal-body">
                                                            <li class="list-group-item">{{$vote->question1}}</li>
                                                            <li class="list-group-item">{{$vote->question2}}</li>
                                                            <li class="list-group-item">{{$vote->question3}}</li>
                                                            <li class="list-group-item">{{$vote->question4}}</li>
                                                            <li class="list-group-item">{{$vote->question5}}</li>
                                                            <li class="list-group-item">{{$vote->question6}}</li>
                                                            <li class="list-group-item">{{$vote->question7}}</li>
                                                            <li class="list-group-item">{{$vote->question8}}</li>
                                                            <li class="list-group-item">{{$vote->question9}}</li>
                                                            <li class="list-group-item">{{$vote->question10}}</li>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @switch($vote->vote_for)
                                            @case("Student")
                                            دانش آموز
                                            @break
                                            @case("Advisor")
                                            مشاور
                                            @break
                                        @endswitch
                                    </td>
                                    <td>{{jdate($vote->created_at)->format('Y/m/d')}}</td>
                                    <td>
                                        @if ($countAnswer == 0)
                                            <a href="{{route('admin.vote.answers', $vote->id)}}" class="btn btn-sm btn-primary disabled">
                                                {{$countAnswer}} پاسخ دهنده
                                            </a>
                                        @else
                                            <a href="{{route('admin.vote.answers', $vote->id)}}" class="btn btn-sm btn-primary">
                                                {{$countAnswer}} پاسخ دهنده
                                            </a>
                                        @endif

                                    </td>
                                    <td class="d-inline-flex">
                                        <a href="{{route('admin.vote.edit', $vote->id)}}" class="btn btn-sm btn-success">ویرایش</a>
                                        <button type="button" class="btn btn-sm btn-danger mr-1" data-toggle="modal" data-target="#exampleModalCenter{{$loop->index}}">
                                            حذف
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModalCenter{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.vote.delete', $vote->id)}}" method="post">
                                                        @csrf
                                                        <div class="modal-body">
                                                            از حذف {{$vote->title}} اطمینان دارید؟
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger">حذف</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection
