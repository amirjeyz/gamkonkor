@extends('admin.layouts.main')

@section('title', 'تنظیمات')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد دوره جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="mt-4">

                    <form action="{{route('admin.settings.set.title')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>عنوان وبسایت</label>
                                <input type="text" name="title" class="form-control" value="{{\App\Models\Setting::where('key', 'title')->first()->value}}">
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.header')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>هدر</label>
                                <input type="text" name="header" class="form-control" value="{{\App\Models\Setting::where('key', 'header')->first()->value}}">
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.subheader')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>ساب هدر</label>
                                <input type="text" name="subheader" class="form-control" value="{{\App\Models\Setting::where('key', 'subheader')->first()->value}}">
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.verysubheader')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>متن زیر ساب هدر</label>
                                <input type="text" name="verysubheader" class="form-control" value="{{\App\Models\Setting::where('key', 'verysubheader')->first()->value}}">
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.cmone')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>نام نظردهنده اول</label>
                                        <input type="text" name="commenter_name1" class="form-control" value="{{\App\Models\Setting::where('key', 'commenter-name1')->first()->value ?? ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>پیام نظردهنده اول</label>
                                        <textarea type="text" name="commenter_msg1" class="form-control" rows="5">
                                            {{\App\Models\Setting::where('key', 'commenter-msg1')->first()->value ?? ''}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.cmtwo')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>نام نظردهنده دوم</label>
                                        <input type="text" name="commenter_name2" class="form-control" value="{{\App\Models\Setting::where('key', 'commenter-name2')->first()->value ?? ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>پیام نظردهنده دوم</label>
                                        <textarea type="text" name="commenter_msg2" class="form-control" rows="5">
                                            {{\App\Models\Setting::where('key', 'commenter-msg2')->first()->value ?? ''}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.cmthree')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>نام نظردهنده سوم</label>
                                        <input type="text" name="commenter_name3" class="form-control" value="{{\App\Models\Setting::where('key', 'commenter-name3')->first()->value ?? ''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>پیام نظردهنده سوم</label>
                                        <textarea type="text" name="commenter_msg3" class="form-control" rows="5">
                                            {{\App\Models\Setting::where('key', 'commenter-msg3')->first()->value ?? ''}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>

                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.contact')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>متن صفحه ارتباط با ما</label>
                                <input type="text" name="contact_text" class="form-control" value="{{\App\Models\Setting::where('key', 'contact-text')->first()->value ?? ''}}">
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>
                    </form>

                    <hr style="width: 95%;">

                    <form action="{{route('admin.settings.set.about')}}" method="POST">
                        @csrf
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>متن صفحه درباره ما</label>
                                <textarea type="text" name="about_us" class="form-control" rows="5">
                                    {!!\App\Models\Setting::where('key', 'about-us')->first()->value ?? ''!!}
                                </textarea>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-sm mb-2 mr-2" type="submit">ثبت تغییرات</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
