@extends('admin.layouts.main')

@section('title', 'داشبورد')


@section('content')

<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">داشبورد مدیریت</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="#">خانه</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <section class="content">
      <div class="container-fluid">
        <div class="row">
            @if(env('APP_ENV') != 'local')
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box bg-info">
                  <span class="info-box-icon">
                    <ion-icon name="mail-outline" style="font-size: 50px;"></ion-icon>
                  </span>

                        <div class="info-box-content">
                            <span class="info-box-text" style="font-size: 18px;font-weight:bolder;">وضعیت پنل پیامکی</span>
                            <span class="info-box-number">اعتبار حساب: {{$ghasedak->balance}} ریال</span>

                            <hr>

                            <span class="progress-description">
                      تاریخ پایان قرارداد:
                        {{jdate(date('Y-m-d', $ghasedak->expire))->format('%d %B، %Y')}}
                    </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @endif

          </div>
      </div>
  </section>

@endsection
