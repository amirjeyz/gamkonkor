@extends('admin.layouts.main')

@section('title', 'ایجاد کاربر')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد کاربر جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>نام و نام خانوادگی کاربر</label>
                            <input type="text" name="name" value="{{old('name')}}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>تلفن تماس کاربر</label>
                            <input type="text" name="phone" value="{{old('phone')}}" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label>نقش کاربر</label>
                            <select name="role" class="form-control">
                                <option value="Student">
                                   دانش آموز
                                </option>
                                <option value="Teacher">
                                   مدرس
                                </option>
                                <option value="Advisor">
                                    مشاور
                                </option>
                                <option value="Admin">
                                   ادمین
                                </option>
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
