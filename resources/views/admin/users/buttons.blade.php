<div class="col-12 ml-2 mb-2">
    <a href="{{route('admin.users.edit', $user)}}"
       class="btn btn-primary mr-1 waves-effect waves-light"><i
            class="feather icon-edit-1"></i> ویرایش</a>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-outline-danger waves-effect waves-light" data-toggle="modal" data-target="#exampleModalCenter">
        حذف {{$role}}
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.users.delete', $user)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        از حذف {{$role}} اطمینان دارید؟
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">حذف</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
