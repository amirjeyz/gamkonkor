@extends('admin.layouts.main')

@section('title', 'تکمیل اطلاعات کاربر')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">تکمیل اطلاعات کاربر</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif
            <!-- form start -->
                <form action="{{route('admin.users.store.with.role',$user)}}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <!-- is Student -->
                        @if($user->hasRole('Student'))

                            <div class="form-group">
                                <label>پایه تحصیلی</label>
                                <input type="text" name="grade" value="{{old('grade')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>رشته&zwnj;ی تحصیلی</label>
                                <input type="text" name="field" value="{{old('field')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>تلفن تماس والدین دانش آموز</label>
                                <input type="text" name="parent_phone" value="{{old('parent_phone')}}"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label>شهر</label>
                                <input type="text" name="city" value="{{old('city')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>ایمیل</label>
                                <input type="email" name="email" value="{{old('email')}}" class="form-control">
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-12 mb-20">
                                    <h6 class="mb-15">عکس جدید</h6>
                                    <div class="row mbn-15">
                                        <input class="dropify" type="file" name="file">
                                    </div>
                                </div>
                            </div>

                            <!-- /.card-body -->

                            <!-- is Advisor -->
                        @elseif($user->hasRole('Advisor'))
                            <div class="form-group">
                                <label>رتبه کنکور</label>
                                <input type="text" name="exam_rank" value="{{old('exam_rank')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>رشته&zwnj;ی تحصیلی</label>
                                <input type="text" name="field" value="{{old('field')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>دانشگاه</label>
                                <input type="text" name="university" value="{{old('university')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>سابقه (سال)</label>
                                <input type="text" name="experience_year" value="{{old('experience_year')}}"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label>ایمیل</label>
                                <input type="email" name="email" value="{{old('email')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>شهر</label>
                                <input type="text" name="city" value="{{old('city')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>آدرس</label>
                                <input type="text" name="address" value="{{old('address')}}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>سابقه (متن)</label>
                                <textarea  name="experience_text" class="form-control">{{old('experience_text')}}</textarea>
                            </div>

                            <div class="form-group">
                                <label>سابقه (فایل)</label>
                                <input type="file" name="experience_file" class="form-control">
                            </div>

                            <!-- is Teacher -->
                        @elseif($user->hasRole('Teacher'))

                        <!-- is Admin -->
                        @elseif($user->hasRole('Admin'))

                        @endif
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
