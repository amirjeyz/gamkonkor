@extends('admin.layouts.main')

@section('title', 'پروفایل کاربر')


@section('content')

    <!-- is Student -->
    @if($user->hasRole('Student'))
        <div class="row mt-3 mx-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">پروفایل کاربر {{$user->name ?? ''}}</h3>
                    </div>
                    <div class="card-body">
                        <img class="card-img-top rounded" style="width:150px;height:150px;object-fit: cover;" src="{{empty($user->student->file) ? '' : asset($user->student->file)}}"  alt="Card image cap">
                        <div class="row mt-1">
                            <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">نام و نام خانوادگی:</td>
                                        <td>&nbsp;{{$user->name ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">پایه تحصیلی:</td>
                                        <td>{{$user->student->grade ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">رشته تحصیلی:</td>
                                        <td>{{$user->student->field ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">تلفن تماس دانش آموز:</td>
                                        <td>&nbsp;{{$user->phone ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">تلفن تماس والدین دانش آموز:</td>

                                        <td>&nbsp;{{$user->student->parent_phone  ?? ''}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 col-md-12 col-lg-5">
                                <table class="ml-0 ml-sm-0 ml-lg-0">
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">شهر:</td>
                                        <td>&nbsp;{{$user->student->city ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">ایمیل:</td>
                                        <td>
                                            @if(empty($user->student->email))
                                                <span class="text-warning"> بدون ایمیل</span>
                                            @else
                                                {{$user->student->email ?? ''}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">نقش:</td>
                                        <td>
                                            دانش آموز
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">ثبت نام در:</td>
                                        <td>
                                            &nbsp; {{jdate($user->created_at)->format('Y-m-d')}}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('admin.users.buttons', ['user' => $user, 'role' => 'دانش آموز'])
                </div>
            </div>
        </div>
        <!-- is Advisor -->
    @elseif($user->hasRole('Advisor'))
        <div class="row mt-3 mx-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">پروفایل کاربر {{$user->name ?? ''}}</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-9 col-md-6 col-lg-5">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">نام و نام خانوادگی:</td>
                                        <td>&nbsp;{{$user->name ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">نام دانشگاه:</td>
                                        <td>&nbsp;{{$user->advisor->university ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">رشته تحصیلی:</td>
                                        <td>{{$user->advisor->field ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">رتبه کنکور:</td>
                                        <td>{{$user->advisor->exam_rank ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">سابقه فعالیت:</td>
                                        <td>{{$user->advisor->experience_year ?? ''}} سال</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">تلفن تماس:</td>
                                        <td>&nbsp;{{$user->phone ?? ''}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 col-md-12 col-lg-5">
                                <table class="ml-0 ml-sm-0 ml-lg-0">
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">شهر:</td>
                                        <td>&nbsp;{{$user->advisor->city ?? ''}}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">ایمیل:</td>
                                        <td>
                                            {{$user->advisor->email ?? ''}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">ثبت نام در:</td>
                                        <td>
                                            &nbsp; {{jdate($user->created_at)->format('Y-m-d')}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">نقش:</td>
                                        <td>
                                            مشاور
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">تعداد دانش آموزان:</td>
                                        <td>
                                            &nbsp;{{$user->students->count() ?? 0}}
                                        </td>
                                    </tr>
                                    @if(!empty($user->advisor->experience_file))
                                        <tr>
                                            <td class="font-weight-bold">سابقه (فایل):</td>
                                            <td>
                                                <form action="{{route('admin.users.download.file')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="path"
                                                           value="{{$user->advisor->experience_file}}">
                                                    <button type="submit"
                                                            class="btn btn-success mr-1 waves-effect waves-light">دانلود
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 col-md-12 col-lg-12">
                                <table class="ml-0 ml-sm-0 ml-lg-0">
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">آدرس:</td>
                                        <td>&nbsp;{{$user->advisor->address ?? ''}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('admin.users.buttons', ['user' => $user, 'role' => 'مشاور'])
                </div>
            </div>
        </div>
        <div class="row mt-3 mx-3">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">لیست دانش آموزان</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>نام و نام خانوادگی</th>
                                <th>شماره تماس</th>
                                <th>تاریخ ثبت نام</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($user->students as $student)
                                <tr>
                                    <td>{{$student->user->name ?? ''}}</td>
                                    <td>{{$student->user->phone ?? ''}}</td>
                                    <td>{{jdate($student->user->created_at)->format('Y-m-d') ?? ''}}</td>
                                    <td>
                                        <a href="{{route('admin.users.show', $student->user->id)}}"
                                           class="btn btn-sm btn-info">مشاهده</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->


            </div>
        </div>
        <!-- is Teacher -->
    @elseif($user->hasRole('Teacher'))

        <!-- is Admin -->
    @elseif($user->hasRole('Admin'))

    @endif

@endsection
