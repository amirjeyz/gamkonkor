@extends('admin.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست کاربران</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control float-right" placeholder="جستجو">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>آیدی</th>
                                <th>نام و نام خانوادگی</th>
                                <th>شماره تماس</th>
                                <th>تاریخ ثبت نام</th>
                                <th>نقش</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{jdate($user->created_at)->format('Y-m-d')}}</td>
                                    <td>
                                        @foreach($user->roles as $role)
                                            @switch($role->name)
                                                @case('Admin')
                                                مدیر
                                                @break
                                                @case('Advisor')
                                                مشاور
                                                @break
                                                @case('Student')
                                                دانش آموز
                                                @break
                                                @case('Teacher')
                                                معلم
                                                @break
                                            @endswitch
                                        @endforeach
                                        @if(! $user->hasRole('Admin') && ! $user->hasRole('Advisor') && ! $user->hasRole('Student') && ! $user->hasRole('Teacher'))
                                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#assignRole-{{$user->id}}">
                                                    نقش
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="assignRole-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header d-flex">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">نقش</h5>
                                                                <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.users.assignRole', $user->id)}}" method="POST">
                                                                @csrf
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label>نقش کاربر</label>
                                                                        <select name="role" class="form-control">
                                                                            <option disabled selected>نقش موردنظر را وارد کنید.</option>
                                                                            <option value="Student">دانش آموز</option>
                                                                            <option value="Advisor">مشاور</option>
                                                                            <option value="Teacher">معلم</option>
                                                                            <option value="Admin">مدیر</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                                    <button type="submit" class="btn btn-sm btn-success mr-1">ثبت</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.users.show', $user)}}" class="btn btn-sm btn-info">مشاهده</a>

                                        <a href="{{route('admin.users.edit', $user)}}" class="btn btn-sm btn-success">ویرایش</a>
                                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModalCenter{{$loop->index}}">
                                            حذف
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModalCenter{{$loop->index}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.users.delete', $user)}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="modal-body">
                                                            از حذف {{$user->name}} اطمینان دارید؟
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger">حذف</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        @if ($user->hasRole('Advisor'))
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#capacityAdvisor-{{$user->id}}">
                                            مدیریت ظرفیت
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="capacityAdvisor-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header d-flex">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">مدیریت ظرفیت مشاور</h5>
                                                        <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.users.addCapacity', $user->id)}}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">

                                                            <div class="form-group">
                                                                <input type="number" name="capacity" class="form-control" value="{{$user->advisor['capacity']}}">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                            <button type="submit" class="btn btn-sm btn-success mr-1">ثبت</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                        @if ($user->hasRole('Advisor'))
                                            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addLessonForAdvisor-{{$user->id}}">
                                                ثبت درس برای مدرس
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="addLessonForAdvisor-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header d-flex">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">مدیریت ظرفیت مشاور</h5>
                                                            <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.users.assignLesson', $user->id)}}" method="POST">
                                                            @csrf
                                                            <div class="modal-body">

                                                                <p>لیست دروس؛</p>
                                                                <ul class="list-group">
                                                                    @foreach(\App\Models\User::find($user->id)->lessons as $lesson)
                                                                        <li class="list-group-item">
                                                                            {{$lesson->name_fa}}
                                                                        </li>
                                                                    @endforeach
                                                                </ul>


                                                                <label class="form-check-label">انتخاب درس:</label>
                                                                <small class="text-danger">حداقل یک درس را انتخاب کنید.</small>
                                                                <div class="form-group">

                                                                    <select class="form-control select2 js-example-basic-multiple" name="lesson_id[]" multiple="multiple"
                                                                            style="width: 100%;color: black;">
                                                                        @foreach(\App\Models\Lesson::all() as $lesson)
                                                                            <option value="{{$lesson->id}}">{{$lesson->name_fa}}</option>
                                                                        @endforeach
                                                                    </select>


                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                                <button type="submit" class="btn btn-sm btn-success mr-1">ثبت</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif


                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#permManagement-{{$user->id}}">
                                            مدیریت دسترسی‌ها
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="permManagement-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header d-flex">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">نقش</h5>
                                                        <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.users.assignPerm', $user->id)}}" method="POST">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <p>دسترسی‌های این کاربر</p>
                                                            <ul class="list-group">
                                                                @foreach(\App\Models\User::find($user->id)->permissions as $perm)
                                                                <li class="list-group-item">
                                                                    {{$perm->name}}
                                                                </li>
                                                                @endforeach
                                                            </ul>

                                                            <label class="mt-4">چند انتخابی</label>
                                                            <div class="form-group">
                                                                <select class="form-control" name="permission">
                                                                    <option value="0" selected disabled>انتخاب کنید</option>
                                                                    @foreach(\Spatie\Permission\Models\Permission::all() as $perm)
                                                                        <option value="{{$perm->name}}">{{$perm->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                            <button type="submit" class="btn btn-sm btn-success mr-1">ثبت</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                        @if($user->hasRole('Student'))

                                            <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#selectAdvisor-{{$user->id}}">
                                                انتخاب مشاور برای دانش آموز
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="selectAdvisor-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header d-flex">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">نقش</h5>
                                                            <button type="button" class="close" style="margin-left: 0;" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.users.addAdvisor', $user->id)}}" method="POST">
                                                            @csrf
                                                            <div class="modal-body">
                                                                <label class="mt-4">انتخاب پکیج</label>
                                                                <div class="form-group">
                                                                    <select class="form-control" name="package">
                                                                        <option value="0" selected disabled>انتخاب کنید</option>
                                                                        @foreach(\App\Models\Package::all() as $package)
                                                                            <option value="{{$package->id}}">{{$package->title}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>

                                                                <label class="mt-4">انتخاب مشاور</label>
                                                                <div class="form-group">
                                                                    <select class="form-control" name="advisor">
                                                                        <option value="0" selected disabled>انتخاب کنید</option>
                                                                        @foreach(\App\Models\Advisor::with('user')->get() as $advisor)
                                                                            <option value="{{$advisor->user->id}}">{{$advisor->user->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">بستن</button>
                                                                <button type="submit" class="btn btn-sm btn-success mr-1">ثبت</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>


                                        @endif
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->




        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $(".js-example-basic-multiple").select2();
        });
    </script>
@endsection
