@extends('admin.layouts.main')

@section('title', 'ایجاد دوره')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">ایجاد واریزی جدید</h3>
                </div>
                <!-- /.card-header -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            <!-- form start -->
                <form action="{{route('admin.bills.storeSalary')}}" method="POST">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label>عنوان دستمزد</label>
                            <input type="text" name="title" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>توضیحات دستمزد</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>


                        <div class="form-group">
                            <label>تاریخ شروع</label>
                            <input type="datetime-local" name="start_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>تاریخ پایان</label>
                            <input type="datetime-local" name="end_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>هزینه واریز شده</label>
                            <input type="number" name="price" class="form-control">
                        </div>


                        <div class="form-group">
                            <label>کاربر موردنظر</label>
                            <select class="form-control" name="user_id">
                                <option value="0" selected disabled>کاربر موردنظر را انتخاب کنید.</option>
                                @foreach($users as $user)
                                    @if ($user->advisor != null)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>


                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ارسال</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
