@extends('admin.layouts.main')

@section('title', 'لیست دوره‌ها')


@section('content')
    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست صورتحساب‌های فروشگاه</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex">
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>نوع محصول</th>
                                <th>نام محصول</th>
                                <th>قیمت</th>
                                <th>نام خریدار</th>
                                <th>وضعیت</th>
                                <th>اقدامات</th>
                            </tr>
                            @foreach($orders as $order)
                            <tr>
                                <td>
                                    @switch($order->type)
                                        @case('course')
                                        دوره آموزشی
                                        @break
                                        @case('package')
                                        پکیج
                                        @break
                                    @endswitch
                                </td>

                                <td>
                                    @switch($order->type)
                                        @case('course')
                                            @foreach($order->courses as $c)
                                                {{$c->title}}
                                            @endforeach
                                        @break
                                        @case('package')
                                            @foreach($order->packages as $p)
                                                {{$p->title}}
                                            @endforeach
                                        @break
                                    @endswitch
                                </td>

                                <td> {{$order->price}} تومان</td>
                                <td>{{$order->user->name}}</td>
                                <td>
                                    @switch($order->status)
                                        @case('paid')
                                        <div class="badge badge-success">پرداخت شده</div>
                                        @break
                                        @case('unpaid')
                                        <div class="badge badge-danger">پرداخت نشده</div>
                                        @break
                                        @case('canceled')
                                        <div class="badge badge-warning">لغو شده</div>
                                        @break
                                    @endswitch
                                </td>
                                <td>
                                    <form action="{{route('admin.bills.deleteOrder', $order->id)}}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-sm btn-danger">حذف</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>


    <div class="row mt-3 mx-3">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">لیست واریزی‌های مشاورین</h3>

                    <div class="card-tools">
                        <div class="input-group input-group-sm d-flex" style="">
                            <a href="{{route('admin.bills.createSalary')}}" class="btn btn-sm btn-success">ایجاد</a>
                        </div>

                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>نام محصول</th>
                            <th>قیمت</th>
                            <th>شروع قرارداد</th>
                            <th>پایان قرارداد</th>
                            <th>کاربر</th>
                            <th>اقدامات</th>
                        </tr>
                        @foreach($salaries as $salary)
                        <tr>
                            <td>{{$salary->title}}</td>
                            <td>{{$salary->price}}</td>
                            <td>{{jdate($salary->start_date)->format('%d %B %Y')}}</td>
                            <td>{{jdate($salary->end_date)->format('%d %B %Y')}}</td>
                            <td>{{$salary->user->name}}</td>
                            <td class="d-flex row">
                                <a href="{{route('admin.bills.editSalary', $salary->id)}}" class="btn btn-sm btn-primary">ویرایش</a>
                                <form action="{{route('admin.bills.deleteSalary', $salary->id)}}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger mr-1 ">حذف</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
