<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        @yield('title') | {{\App\Models\Setting::where('key', 'title')->first()->value}}
    </title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="{{asset('front-files/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('front-files/css/font-awesome.min.css')}}">
</head>

<body>
    @include('home.layouts.nav')

    <!-- content -->
    @yield('content')
    <!--end content-->

    @include('home.layouts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="{{asset('front-files/js/main.js')}}" type="text/javascript "></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.collapse').on('click', 'div:first-child', function() {
                if ($(this).attr('data-collapse') === 'false') {
                    $(this).attr('data-collapse', true)
                    $(this).find('.collapse-arrow-open').show()
                    $(this).find('.collapse-arrow-close').hide()
                    $(this).next().show()
                } else if ($(this).attr('data-collapse') === 'true') {
                    $(this).attr('data-collapse', false)
                    $(this).find('.collapse-arrow-open').hide()
                    $(this).find('.collapse-arrow-close').show()
                    $(this).next().hide()
                }
            })
        })
    </script>
    <script type="text/javascript">
        function menuBtnChange(x) {
            x.classList.toggle("menu-btn-change");
        }
        $(document).ready(function() {
            let mobile_menu = $('.mobile-menu');
            $('.mobile-menu-btn').on('click', function() {
                if ($('.mobile-menu').attr('data-open') === 'false') {
                    mobile_menu.removeClass('hidden animate-left')
                    mobile_menu.addClass('animate-right')
                    mobile_menu.attr('data-open', true)
                } else {
                    mobile_menu.removeClass('animate-right')
                    mobile_menu.addClass('animate-left')
                    setTimeout(function() {
                        mobile_menu.addClass('hidden');
                    }, 4);
                    mobile_menu.attr('data-open', false)
                }
            })
        })
    </script>
    @yield('script')
</body>

</html>
