@extends('home.layouts.master')

@section('title', $course->title)


@section('content')

<section class="">
    <div>
        <div class="bg-gray-50">
            <div class="container">
                <div class="py-5">
                    <h1 class="text-3xl font-bold ">{{$course->title}}</h1>
                </div>
                <div class="flex flex-col md:flex-row space-x-4 space-x-reverse">
                    <div class="w-12/12 md:w-8/12 lg:w-9/12 space-y-4 md:space-y-reverse">
                        <div class="block md:hidden space-y-3">
                            <div class="space-y-3 ">
                                <div class="rounded-lg bg-gray-500 from-gray-500">
                                    <div class="p-4 pattern-2 space-y-4">
                                        <div class="text-white space-y-5 text-base">
                                            <div class="flex justify-between items-center">
                                                <span>قیمت دوره</span>
                                                @php
                                                    $coursePrice = strval($course->price);
                                                    $price = number_format($coursePrice, 0, ',', ',');
                                                    $priceFa = \App\Helpers\EnToFa::convert($price);
                                                @endphp
                                                <span>{{$priceFa}} تومان</span>
                                            </div>
                                            <div class="flex justify-between items-center">
                                                <span class="font-bold">وضعیت دوره</span>
                                                <span class="bg-green-400 text-white px-1 py-1 rounded text-sm font-medium">
                                                    @switch($course->status)
                                                        @case('uncomplete')
                                                            درحال ضبط
                                                            @break
                                                        @case('completed')
                                                            تکمیل شده
                                                            @break;
                                                    @endswitch
                                                </span>
                                            </div>
                                            <div class="flex justify-between items-center more-margin-b">
                                                <span class="font-bold">زمان کل دوره</span>
                                                <span>
                                                    @php
                                                        $courseTime = strval($course->course_time);
                                                        $timeFa = \App\Helpers\EnToFa::convert($courseTime);
                                                    @endphp
                                                    {{$timeFa}}
                                                </span>
                                            </div>
                                            <div class="flex justify-between items-center">
                                                <span class="font-bold">تعداد قسمت‌ها</span>
                                                <span>
                                                    @php
                                                        $episodesCount = strval(count($course->episodeHa));
                                                        $epFa = \App\Helpers\EnToFa::convert($episodesCount);
                                                    @endphp
                                                    {{$epFa}}
                                                </span>
                                            </div>
                                            <div class="flex justify-between items-center">
                                                <span class="font-bold">مدرس</span>
                                                <span>
                                                    {{$course->teacher_name}}
                                                </span>
                                            </div>

                                        </div>
                                        <hr class="opacity-20 my-4 border-gray-100">
                                        <div class="space-y-2">
                                            <div class="bg-white text-base flex justify-center items-center rounded-lg font-medium hover:bg-gray-300 hover:text-gray-700 ">
                                                @if (Auth::check())
                                                    <div class="bg-white text-base flex justify-center items-center rounded-lg font-medium hover:bg-gray-300 hover:text-gray-700 ">
                                                        {{-- <a class="w-full text-center p-4" href="/login">خرید دوره</a> --}}
                                                        <button href="javascript:void(0)" @click="showModal = true" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded">خرید دوره</button>
                                                    </div>
                                                @else
                                                    <div class="bg-white text-base flex justify-center items-center rounded-lg font-medium hover:bg-gray-300 hover:text-gray-700 ">
                                                        <a class="w-full text-center p-4" href="/login">ابتدا وارد سایت شوید</a>
                                                    </div>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="bg-white rounded-lg shadow-sm border border-solid border-gray-200">
                            <div class="flex items-center justify-between border-b border-solid border-gray-800 border-opacity-10 px-4 pt-2">
                                <div class="flex flex-col pt-4 sm:flex-row justify-center items-start sm:items-center pr-3 sm:space-x-8 sm:space-x-reverse">
                                    <span class="pb-4 sm:px-2 font-bold sm:border-b-3 sm:border-yellow-400 border-solid text-base relative top-0.5">توضیحات</span>
                                    <a class="pb-4 text-base font-medium" href="#episodes_list">جلسات دوره</a>
                                </div>
                            </div>
                            <div id="content-area" class="content-area px-6 py-4 loadmore">
                                {!!$course->content!!}
                            </div>

                        </div>

                        <div id="episodes_list">
                            <span class="text-2xl font-bold text-gray-600 mb-4 block">جلسات دوره</span>
                            <div class="bg-white rounded-lg shadow-sm border border-solid border-gray-200 overflow-hidden">
                                <div class="collapse">

                                    @foreach ($course->episodeHa as $episode)
                                        <!-- free episodes -->
                                        <div class="flex justify-between items-center px-8 py-6 border-b border-solid border-gray-200 hover:bg-gray-50 ">
                                            <div class="flex items-center space-x-2 space-x-reverse">
                                                <span class="w-11 h-11 rounded-full flex-shrink-0 flex justify-center items-center text-lg font-bold border-3 border-solid text-white bg-gray-500 border-gray-400">
                                            <span class="pt-1">۱</span>
                                                </span>
                                                <div class="space-y-3 ">
                                                    <a class="text-lg font-medium hover:underline leading-normal" href="#">
                                                        {{$episode->title}}
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="hidden sm:block">
                                                <div class="flex items-center space-x-reverse space-x-1">

                                                    @if (Auth::check())
                                                        @php
                                                            $orders = \App\Models\Order::where('type', 'course')->where('user_id', auth()->user()->id)->where('status', 'paid')->get();
                                                            $array = [];
                                                            foreach ($orders as $order) {
                                                                foreach ($order->courses as $key => $value) {
                                                                    $array[] = $value->id;
                                                                }
                                                            }
                                                        @endphp
                                                    @endif

                                                    @if ($episode->price > 0 && ! in_array($course->id,$array) )
                                                        <span class="text-white text-sm p-1 px-2 rounded font-medium bg-red-500 border-green-700">
                                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 28 28" stroke="currentColor" class="w-5 h-5">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                                            </svg>
                                                        </span>
                                                    @elseif (in_array($course->id,$array))
                                                        <a href="{{route('courses.episode', ['course' => $course->id, 'episode' => $episode->id])}}" class="text-white text-sm p-1 px-2 rounded font-medium bg-gray-300 text-gray-600 border-green-700">
                                                            مشاهده
                                                        </a>
                                                    @else
                                                        <span class="text-white text-sm p-1 px-2 rounded font-medium bg-gray-300 text-gray-600 border-green-700">رایگان</span>
                                                    @endif
                                                    <span class="text-white text-sm p-1 px-2 rounded font-medium bg-gray-500 border-gray-400 ">۰۳:۲۵</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- free episodes -->
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="w-12/12 md:w-4/12 lg:w-3/12 hidden md:block space-y-3">
                        <div class="space-y-3 mb-3">
                            <div class="rounded-lg bg-gray-500 from-gray-500">
                                <div class="p-4 pattern-2 space-y-4">
                                    <div class="text-white space-y-5 text-base">
                                        <div class="flex justify-between items-center">
                                            <span>قیمت دوره</span>
                                            @php
                                                $coursePrice = strval($course->price);
                                                $price = number_format($coursePrice, 0, ',', ',');
                                                $priceFa = \App\Helpers\EnToFa::convert($price);
                                            @endphp
                                            <span>{{$priceFa}} تومان</span>
                                        </div>
                                        <div class="flex justify-between items-center">
                                            <span class="font-bold">وضعیت دوره</span>
                                            <span class="bg-green-400 text-white px-1 py-1 rounded text-sm font-medium">
                                                @switch($course->status)
                                                    @case('uncomplete')
                                                        درحال ضبط
                                                        @break
                                                    @case('completed')
                                                        تکمیل شده
                                                        @break;
                                                @endswitch
                                            </span>
                                        </div>
                                        <div class="flex justify-between items-center more-margin-b">
                                            <span class="font-bold">زمان کل دوره</span>
                                            <span>
                                                @php
                                                    $courseTime = strval($course->course_time);
                                                    $timeFa = \App\Helpers\EnToFa::convert($courseTime);
                                                @endphp
                                                {{$timeFa}}
                                            </span>
                                        </div>
                                        <div class="flex justify-between items-center">
                                            <span class="font-bold">تعداد قسمت‌ها</span>
                                            <span>
                                                @php
                                                    $episodesCount = strval(count($course->episodeHa));
                                                    $epFa = \App\Helpers\EnToFa::convert($episodesCount);
                                                @endphp
                                                {{$epFa}}
                                            </span>
                                        </div>
                                        <div class="flex justify-between items-center">
                                            <span class="font-bold">مدرس</span>
                                            <span>

                                                {{$course->teacher_name}}
                                            </span>
                                        </div>
                                    </div>
                                    <hr class="opacity-20 my-4 border-gray-100">


                                    <div class="space-y-2">


                                        @if (Auth::check() && ! in_array($course->id,$array))
                                        <div class="bg-white text-base flex justify-center items-center font-medium py-2 px-4 rounded-lg hover:bg-gray-300 transition duration-150 ease-in-out">
                                            <button id="myBtn" class="w-full text-center p-1"> خرید دوره</button>
                                        </div>

                                        <!-- The Modal -->
                                        <div id="myModal" class="modal" style="z-index: 99999;">

                                            <!-- Modal content -->
                                            <div class="modal-content rounded-md p-12 ">
                                                <span class="close">×</span>
                                                <h1 class="font-black text-lg text-black text-center" style="font-size: 25px;">
                                                    خرید :
                                                    {{$course->title}}
                                                </h1>
                                                <div class=" flex justify-center flex-col rounded-md p-6 ">
                                                    <div class="font-bold text-lg border-b border-gray-200 border-solid flex justify-between">
                                                        <span>قیمت دوره</span>
                                                        <span class="text-green-600">
                                                            @php
                                                            $coursePrice = strval($course->price);
                                                            $price = number_format($coursePrice, 0, ',', ',');
                                                            $priceFa = \App\Helpers\EnToFa::convert($price);
                                                            @endphp
                                                            {{$priceFa}} تومان
                                                        </span>
                                                    </div>

                                                    <form action="{{route('course.payment')}}" method="POST" class=" flex justify-start items-start flex-col mt-8 ">
                                                        @csrf
                                                        <input type="hidden" name="course_id" value="{{$course->id}}">
                                                        <button type="submit" class="bg-blue-500 hover:bg-blue-400 text-white p-3 px-4 font-medium rounded-lg hover:text-white" style="font-size:20px;">
                                                            پرداخت
                                                        </button>
                                                    </form>

                                                    <div class="mt-5 bg-gray-100 rounded-lg w-full p-4 text-tiny leading-loose">
                                                        <span class="font-bold text-red-600">پرداخت از درگاه بانک با استفاده از کلیه کارت‌های عضو شتاب</span>
                                                        <p class="text-sm text-gray-500"></p>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        @elseif (in_array($course->id,$array))
                                            <div class="bg-red-500 text-white flex justify-center items-center font-medium py-2 px-4 rounded-lg transition duration-150 ease-in-out">
                                                <span class="w-full text-center p-1">شما به این دوره دسترسی دارید</span>
                                            </div>
                                        @else
                                            <div class="bg-white text-base flex justify-center items-center rounded-lg font-medium hover:bg-gray-300 hover:text-gray-700 ">
                                                <a class="w-full text-center p-4" href="/auth/login">ابتدا وارد شوید</a>
                                            </div>

                                        @endif

                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('.collapse').on('click', 'div:first-child', function () {
            if ($(this).attr('data-collapse') === 'false') {
                $(this).attr('data-collapse', true)
                $(this).find('.collapse-arrow-open').show()
                $(this).find('.collapse-arrow-close').hide()
                $(this).next().show()
            } else if ($(this).attr('data-collapse') === 'true') {
                $(this).attr('data-collapse', false)
                $(this).find('.collapse-arrow-open').hide()
                $(this).find('.collapse-arrow-close').show()
                $(this).next().hide()
            }
        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function () {
            $('.openModal').on('click', function(e){
                $('#interestModal').removeClass('invisible');
            });
            $('.closeModal').on('click', function(e){
                $('#interestModal').addClass('invisible');
            });
        });
</script>
@endsection
