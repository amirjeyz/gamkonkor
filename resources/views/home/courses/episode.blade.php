@extends('home.layouts.master')

@section('title', $episode->title)

@section('content')
<div class="bg-gray-100 pt-6 pb-12 text-gray-600">
    <div class="container space-y-4 mb-12">
        <div>
            <h1 class="text-3xl font-bold">{{$episode->title}}</h1>
        </div>
        <div class="font-bold text-gray-500">
            <a class="text-gray-600 hover:underline" href="/series/learn-html">{{$course->title}}</a>
        </div>
        <div class="flex flex-wrap space-y-2 sm:space-y-0 sm:space-x-2 sm:space-x-reverse text-white">
            <div class="bg-gray-600 p-1 px-2 rounded leading-normal">
                <span class="font-bold">زمان قرارگیری : </span>
                <span class="item_section">
                    {{\App\Helpers\EnToFa::convert(jdate($episode->created_at)->format('%d %B، %Y'))}}
                </span>
            </div>
        </div>
        <div class="flex justify-center" id="container">
            <video class="rounded-md shadow-lg" controls crossorigin playsinline data-poster="{{$episode->cover}}" id="player">
              <!-- Video files -->
              <source src="{{$episode->video_link}}" type="video/mp4" size="720"/>
            </video>
        </div>
        </main>
    </div>

</div>
@endsection
