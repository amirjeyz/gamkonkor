@extends('home.layouts.master')

@section('title', 'دوره‌ها')

@section('content')
<section class="">
    <div class="flex-1 pt-3 pb-8 bg-gray-200">
        <div class="container">
            <div class="flex justify-between items-center flex-wrap mb-4 pt-2">
                <a href="#" class="text-2xl font-bold text-gray-600 block">جدیدترین دوره‌ها</a>
            </div>

            <div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 grid-flow-row gap-4">
                @foreach ($courses as $course)
                    <div wire:id="kw92pYOoCnOSTO6vQILW">
                        <article class="flex flex-col h-full rounded-lg bg-white bg-white shadow-sm overflow-hidden">
                            <div class="h-48 overflow-hidden relative m-3 rounded-lg">

                                <img class="w-full h-full object-cover lozad" data-src="{{$course->cover}}" alt="{{$course->title}}" src="{{$course->cover}}"
                                    data-loaded="true">
                            </div>
                            <div class="flex flex-col flex-1 space-y-3">
                                <div class="px-4 flex-grow">
                                    <h2 class="font-medium text-lg leading-relaxed tracking-tight hover:underline"><a href="{{route('courses.single‍', $course->id)}}"><span>{{$course->title}}</span></a></h2>
                                    <p class="mt-2 mb-3 text-sm opacity-70 leading-relaxed">
                                        {{\Illuminate\Support\Str::limit($course->description, 100, '...')}}
                                    </p>
                                </div>
                                <div>
                                    <div class="bg-gray-50 text-gray-600 rounded-lg py-4 px-5 ">
                                        <div class="flex justify-between items-center">
                                            <div class="flex items-baseline font-bold flex-row-reverse text-tiny">
                                                <div class="flex items-center space-x-2 space-x-reverse">
                                                    <span>{{$course->course_time}}</span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="currentColor" class="w-5 h-5">
                                                        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z" class=""></path>
                                                    </svg> </div>
                                            </div>
                                            <span class="translate font-bold text-green-600">{{$course->price}} تومان</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

@endsection

