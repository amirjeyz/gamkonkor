@extends('home.layouts.master')

@section('title', 'ارتباط با ما')

@section('content')
<section class="bg-gray-100  flex justify-center">
    <div class="box-contact bg-white rounded-md shadow-lg mx-8 my-12 ">
        <div class="content p-6 ">
            <div class="contact-header">
                <h1 class="text-blue-500 mb-2 font-block">ارتباط باما</h1>
                <p>
                    {{\App\Models\Setting::where('key', 'contact-text')->first()->value ?? ''}}
                </p>
            </div>
            <div class="contact-main mt-4">
                <form class=" grid grid-cols-1 " method="post" action="">
                    <div class="content-form grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
                        <div class="group flex flex-col">
                            <label for="name">نام و نام خانوادگی:</label>
                            <input class="rounded-md mt-2 outline-none bg-gray-100 w-64 p-2" type="text" placeholder="میثاق رمضانی">
                        </div>
                        <div class="group flex flex-col">
                            <label for="number">شماره تماس:</label>
                            <input class="rounded-md mt-2 outline-none bg-gray-100 w-64 p-2" type="text" placeholder="09330447752 ">
                        </div>
                        <div class="group flex flex-col">
                            <label for="goal">موضوع:</label>
                            <input class="rounded-md mt-2 outline-none bg-gray-100 w-64 p-2" type="text" placeholder="طراحی سایت ">
                        </div>
                    </div>
                    <div class="group mt-6  flex flex-col">
                        <label for="goal">توضیحات:</label>
                        <textarea class="rounded-md mt-2 outline-none bg-gray-100 w-full p-2" type="text" placeholder="طراحی سایت "></textarea>
                    </div>

                    <div class="group mt-6 mb-6 flex justify-end">
                        <button class="bg-blue-500 mr-12 hover:bg-blue-600 text-white px-4 py-2 shadow-lg rounded-md flex justify-center">ارسال تیکت</button>

                    </div>
            </div>
        </div>
</section>

@endsection

