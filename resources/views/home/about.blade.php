@extends('home.layouts.master')

@section('content')
<section class="bg-gray-100  flex justify-center">
    <div class="box-contact bg-white rounded-md shadow-lg mx-8 my-12 ">
        <div class="content p-6 ">
            <div class="contact-header">
                <h1 class="text-blue-500 mb-2 font-block">درباره ما</h1>
                <p>

                </p>
            </div>
            <div class="contact-main mt-4 text-right">
                {!!\App\Models\Setting::where('key', 'about-us')->first()->value ?? ''!!}
            </div>
        </div>
</section>

@endsection

