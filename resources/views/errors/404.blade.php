<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>اررور ۴۰۴ | گام کنکور</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="{{asset('front-files/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('front-files/css/font-awesome.min.css')}}">
</head>

<body>
    <div class="contain">

        <article class=" flex justify-center flex-col bg-white rounded-lg mx-12 my-12">
            <div class="cover flex justify-center">
                <img src="{{asset('front-files/img/95483df50a0a3324c4cf9ccb1094b825.webp')}}" alt="">
            </div>
            <h1 class="text-gray-900 text-4xl mb-6 font-black text-center">اوپسسس، پیدا نشد!!!</h1>
            <div class="flex justify-center mb-6"> <a class="text-center hover:border-gray-500 transition duration-500 ease-in-out block w-max border border-gray-200 rounded-lg px-3 p-3" href="http://gamkonkor.ir/">بازگشت به صفحه اصلی</a></div>
        </article>
    </div>
</body>

</html>
