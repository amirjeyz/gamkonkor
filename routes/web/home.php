<?php

use App\Helpers\EnToFa;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Ghasedak\GhasedakApi;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Home\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'auth'], function() {
    Route::get('login', [\App\Http\Controllers\Auth\LoginController::class, 'showLogin'])->name('auth.login');
    Route::post('login', [\App\Http\Controllers\Auth\LoginController::class, 'storeLogin'])->name('auth.login.store');
    Route::get('register', [\App\Http\Controllers\Auth\RegisterController::class, 'showRegister'])->name('auth.register');
    Route::post('register', [\App\Http\Controllers\Auth\RegisterController::class, 'storeRegister'])->name('auth.register.store');
    Route::get('token', [\App\Http\Controllers\Auth\TokenController::class, 'showToken'])->name('auth.phone.token');
    Route::post('token', [\App\Http\Controllers\Auth\TokenController::class, 'storeToken'])->name('auth.phone.storeToken');
    Route::post('logout', function(){
        Auth::logout();
        return redirect('/');
    })->name('auth.logout');
});

/// Shop Routes | Amir ///
Route::group(['prefix' => 'courses'], function() {
    Route::get('/', [\App\Http\Controllers\Home\CourseController::class, 'index'])->name('courses.index');
    Route::get('/{id}', [\App\Http\Controllers\Home\CourseController::class, 'courseSingle'])->name('courses.single‍');
    Route::get('/{course}/episode/{episode}', [\App\Http\Controllers\Home\CourseController::class, 'singleEpisode'])->name('courses.episode');
});



Route::get('contact', function() {
    return view('home.contact');
})->name('home.contact');

Route::get('about-us', function() {
    return view('home.about');
})->name('home.about');


Route::post('/payment', [\App\Http\Controllers\Home\PaymentController::class, 'payment'])->name('course.payment');
Route::get('callback', [\App\Http\Controllers\Home\PaymentController::class, 'callback'])->name('payment.callback');
Route::get('callbacktest', [\App\Http\Controllers\Home\PaymentController::class, 'callback_test']);

//////// Web API /////////
Route::post('webapi/exam/{quiz}/store', [\App\Http\Controllers\Api\ExamController::class, 'storeAnswers'])->name('webapi.quiz.store');

Route::get('amir/login', function(Request $request){
    Auth::loginUsingId($request->role);
    return Auth::user();
});


Route::get('karname', [\App\Http\Controllers\Student\ExamController::class, 'karname']);
