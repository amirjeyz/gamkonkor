<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Advisor Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\AdvisorController::class, 'index'])->name('advisor.index');
Route::get('/student/{user}', [\App\Http\Controllers\AdvisorController::class, 'studentProfile'])->name('advisor.studentProfile');
Route::get('profile', [App\Http\Controllers\AdvisorController::class, 'profile'])->name('advisor.profile');
Route::get('profile/edit', [App\Http\Controllers\AdvisorController::class, 'edit'])->name('advisor.profile.edit');
Route::patch('profile/update/{user}', [App\Http\Controllers\AdvisorController::class, 'update'])->name('advisor.profile.update');
Route::get('profile/new-password', [App\Http\Controllers\AdvisorController::class, 'newPassword'])->name('advisor.profile.new.password');
Route::patch('profile/update-password/{user}', [App\Http\Controllers\AdvisorController::class, 'updatePassword'])->name('advisor.profile.update.password');

Route::get('salaries', [\App\Http\Controllers\Advisor\BillsController::class, 'index'])->name('salaries.index');

Route::get('exam', [App\Http\Controllers\Advisor\ExamController::class, 'index'])->name('advisor.exam.index');
Route::get('exam/create', [App\Http\Controllers\Advisor\ExamController::class, 'create'])->name('advisor.exam.create');
Route::post('exam/create', [App\Http\Controllers\Advisor\ExamController::class, 'store'])->name('advisor.exam.store');
Route::post('exam/{id}/start', [App\Http\Controllers\Advisor\ExamController::class, 'startExam'])->name('advisor.exam.start');
Route::post('exam/{id}/stop', [App\Http\Controllers\Advisor\ExamController::class, 'stopExam'])->name('advisor.exam.stop');
Route::post('exam/{id}/delete', [App\Http\Controllers\Advisor\ExamController::class, 'delete'])->name('advisor.exam.delete');
