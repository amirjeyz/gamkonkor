<?php

use App\Http\Controllers\Admin\ExamController;
use App\Http\Controllers\Admin\SettingController;
use App\Models\User;
use App\Notifications\SendVoteNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment as ShetabitPayment;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Admin\IndexController::class, 'index'])->name('home');

Route::get('courses/episode/{id}/add', 'Admin\EpisodeController@showAddEpisode')->name('episode.add');
Route::post('courses/episode/{id}/add', 'Admin\EpisodeController@storeAddEpisode')->name('episode.add.store');
Route::post('courses/episode/{id}/delete', 'Admin\EpisodeController@deleteEpisode')->name('episode.delete');
Route::get('courses/episode/{episode}/edit', 'Admin\EpisodeController@edit')->name('episode.edit');
Route::post('courses/episode/{episode}/edit', 'Admin\EpisodeController@update')->name('episode.update');
Route::resource('courses', 'Admin\CourseController');

Route::get('user/create', [\App\Http\Controllers\Admin\UserController::class, 'create'])->name('users.create');
Route::get('user/create-with-role/{user}', [\App\Http\Controllers\Admin\UserController::class, 'createWithRole'])->name('users.create.with.role');
Route::post('user/store-with-role/{user}', [\App\Http\Controllers\Admin\UserController::class, 'storeWithRole'])->name('users.store.with.role');
Route::prefix('user')->group(function () {
    Route::get('list', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('users.list');
    Route::get('J/{user}', [\App\Http\Controllers\Admin\UserController::class, 'show'])->name('users.show');
    Route::get('create', [\App\Http\Controllers\Admin\UserController::class, 'create'])->name('users.create');
    Route::post('store', [\App\Http\Controllers\Admin\UserController::class, 'store'])->name('users.store');
    Route::get('edit/{user}', [\App\Http\Controllers\Admin\UserController::class, 'edit'])->name('users.edit');
    Route::patch('update/{user}', [\App\Http\Controllers\Admin\UserController::class, 'update'])->name('users.update');
    Route::delete('delete/{user}', [\App\Http\Controllers\Admin\UserController::class, 'delete'])->name('users.delete');
    Route::post('{user}/assignRole', [\App\Http\Controllers\Admin\UserController::class, 'assignRole'])->name('users.assignRole');
    Route::post('download-file', [\App\Http\Controllers\Admin\UserController::class, 'getDownload'])->name('users.download.file');
    Route::post('{user}/assignPerm', [\App\Http\Controllers\Admin\UserController::class, 'assignPerm'])->name('users.assignPerm');
    Route::post('{user}/addCapacity', [\App\Http\Controllers\Admin\UserController::class, 'addCapacity'])->name('users.addCapacity');
    Route::post('{user}/addAdvisor', [\App\Http\Controllers\Admin\UserController::class, 'addAdvisorToStudent'])->name('users.addAdvisor');
    Route::post('{user}/assignLesson', [\App\Http\Controllers\Admin\UserController::class, 'assignLessonToAdvisor'])->name('users.assignLesson');
});

Route::prefix('vote')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\VoteController::class, 'index'])->name('vote.index');
    Route::get('/create', [\App\Http\Controllers\Admin\VoteController::class, 'create'])->name('vote.create');
    Route::post('/create', [\App\Http\Controllers\Admin\VoteController::class, 'store'])->name('vote.store');
    Route::get('{id}/answers', [\App\Http\Controllers\Admin\VoteController::class, 'getAnswers'])->name('vote.answers');
    Route::get('/{vote}/edit', [\App\Http\Controllers\Admin\VoteController::class, 'edit'])->name('vote.edit');
    Route::post('/{vote}/edit', [\App\Http\Controllers\Admin\VoteController::class, 'update'])->name('vote.update');
    Route::post('/{id}/delete', [\App\Http\Controllers\Admin\VoteController::class, 'delete'])->name('vote.delete');
});

Route::prefix('packages')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\PackagesController::class, 'index'])->name('packages.index');
    Route::get('/create', [\App\Http\Controllers\Admin\PackagesController::class, 'create'])->name('packages.create');
    Route::post('/create', [\App\Http\Controllers\Admin\PackagesController::class, 'store'])->name('packages.store');
    Route::get('/{package}/edit', [\App\Http\Controllers\Admin\PackagesController::class, 'edit'])->name('packages.edit');
    Route::post('/{package}/edit', [\App\Http\Controllers\Admin\PackagesController::class, 'update'])->name('packages.update');
    Route::post('/{id}/delete', [\App\Http\Controllers\Admin\PackagesController::class, 'delete'])->name('packages.delete');
});

Route::prefix('bills')->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\BillsController::class, 'index'])->name('bills.index');
    Route::get('/createSalary', [\App\Http\Controllers\Admin\BillsController::class, 'createSalary'])->name('bills.createSalary');
    Route::post('/createSalary', [\App\Http\Controllers\Admin\BillsController::class, 'storeSalary'])->name('bills.storeSalary');
    Route::get('/{salary}/editSalary', [\App\Http\Controllers\Admin\BillsController::class, 'editSalary'])->name('bills.editSalary');
    Route::post('/{salary}/editSalary', [\App\Http\Controllers\Admin\BillsController::class, 'updateSalary'])->name('bills.updateSalary');
    Route::post('/{salary}/deleteSalary', [\App\Http\Controllers\Admin\BillsController::class, 'deleteSalary'])->name('bills.deleteSalary');
    Route::post('/{order}/deleteOrder', [\App\Http\Controllers\Admin\BillsController::class, 'deleteOrder'])->name('bills.deleteOrder');
});


Route::prefix('settings')->group(function () {
    Route::get('/', [SettingController::class, 'index'])->name('settings');
    Route::post('/set/title', [SettingController::class, 'setTitle'])->name('settings.set.title');
    Route::post('/set/header', [SettingController::class, 'setHeader'])->name('settings.set.header');
    Route::post('/set/subheader', [SettingController::class, 'setSubHeader'])->name('settings.set.subheader');
    Route::post('/set/verysubheader', [SettingController::class, 'setVerySubHeader'])->name('settings.set.verysubheader');
    Route::post('/set/cmOne', [SettingController::class, 'setCommentOne'])->name('settings.set.cmone');
    Route::post('/set/cmTwo', [SettingController::class, 'setCommentTwo'])->name('settings.set.cmtwo');
    Route::post('/set/cmThree', [SettingController::class, 'setCommentThree'])->name('settings.set.cmthree');
    Route::post('/set/contact', [SettingController::class, 'setContact'])->name('settings.set.contact');
    Route::post('/set/about', [SettingController::class, 'setAboutUs'])->name('settings.set.about');

});

Route::prefix('exam')->group(function () {
    Route::get('/', [ExamController::class, 'index'])->name('exam.index');

    Route::get('create', [ExamController::class, 'create'])->name('exam.create');
    Route::post('create', [ExamController::class, 'store'])->name('exam.store');
    Route::post('ckeditor', [ExamController::class, 'uploadImage'])->name('exam.ckeditor');
    Route::post('/{id}/start', [ExamController::class, 'startExam'])->name('exam.start');
    Route::post('/{id}/stop', [ExamController::class, 'stopExam'])->name('exam.stop');
    Route::post('/{id}/delete', [ExamController::class, 'delete'])->name('exam.delete');
    Route::get('/lessons', [ExamController::class, 'showLessons'])->name('exam.lesson');
    Route::post('/lessons', [ExamController::class, 'storeLesson'])->name('exam.lesson.store');
});
