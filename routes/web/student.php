<?php

use App\Http\Controllers\Student\CourseController;
use App\Http\Controllers\Student\ExamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Student Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Student\IndexController::class, 'index'])->name('index')->middleware('student.profile');

Route::prefix('vote')->group(function () {
    Route::get('{id}', [\App\Http\Controllers\Student\VoteController::class, 'vote'])->name('vote.index')->middleware('student.profile');
    Route::post('{id}/submit', [\App\Http\Controllers\Student\VoteController::class, 'submit'])->name('vote.submit')->middleware('student.profile');
});

Route::get('profile', [App\Http\Controllers\Student\StudentController::class, 'profile'])->name('profile')->middleware('student.profile');
Route::get('profile/edit', [App\Http\Controllers\Student\StudentController::class, 'edit'])->name('profile.edit');
Route::patch('profile/update/{user}', [App\Http\Controllers\Student\StudentController::class, 'update'])->name('profile.update');
Route::get('profile/new-password', [App\Http\Controllers\Student\StudentController::class, 'newPassword'])->name('profile.new.password')->middleware('student.profile');
Route::patch('profile/update-password/{user}', [App\Http\Controllers\Student\StudentController::class, 'updatePassword'])->name('profile.update.password')->middleware('student.profile');

Route::get('notification/{id}/redirect', [\App\Http\Controllers\Student\NotificationController::class, 'redirect'])->name('notify.redirect')->middleware('student.profile');

Route::prefix('bills')->group(function () {
    Route::get('/', [\App\Http\Controllers\Student\BillsController::class, 'index'])->name('bills.index')->middleware('student.profile');
    Route::post('/{id}/sentToPayment', [\App\Http\Controllers\Student\BillsController::class, 'billPayment'])->name('bills.payment')->middleware('student.profile');
    Route::get('callback', [\App\Http\Controllers\Student\BillsController::class, 'billCallback'])->name('bills.callback')->middleware('student.profile');
});


Route::prefix('/chat')->group(function() {
    Route::get('/', [\App\Http\Controllers\Student\ChatController::class, 'list'])->name('chat.list')->middleware('student.profile');

    Route::get('/{ticket_id}', [\App\Http\Controllers\Student\ChatController::class, 'index'])->name('chat.index')->middleware('student.profile');
    Route::post('/sendMessage/{ticket_id}', [\App\Http\Controllers\Student\ChatController::class, 'sendMessage'])->name('chat.sendMessage')->middleware('student.profile');
});

Route::prefix('/package')->group(function() {
    Route::get('/buy', [\App\Http\Controllers\Student\PackageController::class, 'index'])->name('package.store')->middleware('student.profile');
    Route::post('{package}/buy', [\App\Http\Controllers\Student\PackageController::class, 'storeBuy'])->name('package.store.buy')->middleware('student.profile');
    Route::get('/callback/{advisorID}/{psychologist}/{teacher}', [\App\Http\Controllers\Student\PackageController::class, 'Callback'])->name('package.callback')->middleware('student.profile');
    Route::get('/callbacktest/{advisorID}/{psychologist}/{teacher}', [\App\Http\Controllers\Student\PackageController::class, 'CallbackTest'])->name('package.callback.test')->middleware('student.profile');
});

Route::get('/courses', [CourseController::class, 'index'])->name('courses.index')->middleware('student.profile');


Route::prefix('exam')->group(function() {
    Route::get('/', [ExamController::class, 'index'])->name('exam.index')->middleware('student.profile');
    Route::get('/{id}', [ExamController::class, 'showSingle'])->name('exam.single')->middleware('student.profile');
});
