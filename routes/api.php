<?php

use App\Http\Controllers\Api\ExamController;
use Carbon\Carbon;
use Ghasedak\GhasedakApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('exam/{quiz}/questions', [\App\Http\Controllers\Api\ExamController::class, 'getQuestions']);
Route::get('exam/{quiz}/time', [\App\Http\Controllers\Api\ExamController::class, 'remainingTime']);

Route::post('create', [\App\Http\Controllers\Admin\ExamController::class, 'store']);

Route::post('dev', [\App\Http\Controllers\DevelopController::class, 'dev']);

Route::get('get-teachers/{lesson}', [\App\Http\Controllers\Api\StudentController::class, 'getTeachers']);
